/*jshint esversion: 8 */

import { main_svg, beer_colors, beer_names, scale, width, height, cap_size, beer_styles, beer_tastes, abv_distribution, beer_abvs, score_distribution, beer_scores } from "./visualisations.js";

export const axis_width = width / 4;
export const axis_height = height / 5;
export const radar_width = width * 0.43;

// function updateBeerVisualisation(beer) : draws the visualisations around the beer
export function updateBeerVisualisation(beer) {
  drawBeerName(main_svg, beer);
  drawBeerStyle(main_svg, beer);
  const beer_color = determineBeerColor(beer_colors[beer.index]);
  drawBeerTaste(beer, beer_color);
  drawBeerAlcoholContent(beer, beer_color);
  drawBeerScore(beer, beer_color);
}

// function drawBeerName(svg, beer) : draws the beer name in svg above the beer
function drawBeerName(svg, beer) {
  svg
    .append("text")
    .text(beer_names[beer.index])
    .attr("id", "text-id")
    .attr("class", "text" + " " + "vis" + beer.index)
    .style("font-size", "40" / scale)
    .style("text-anchor", "middle")
    .attr("x", beer.x + cap_size * 0.5)
    .attr("y", beer.y - cap_size * 0.25);
}

// function drawBeerStyle(svg, beer) : draws the beer style in svg above the beer under the name
function drawBeerStyle(svg, beer) {
  svg
    .append("text")
    .text(beer_styles[beer.index].value)
    .attr("id", "text-id")
    .attr("class", "text" + " " + "vis" + beer.index)
    .style("text-anchor", "middle")
    .style("font-size", "25" / scale)
    .attr("x", beer.x + cap_size * 0.5)
    .attr("y", beer.y - cap_size * 0.1);
}

// function drawBeerTaste(beer, beer_color) : draws the beer style in svg under the beer in the color of the beer
function drawBeerTaste(beer, beer_color) {
  RadarChart.draw("vis" + beer.index, main_svg, [beer_tastes[beer.index]], {
    max_value: 6,
    data_levels: 6,
    translate_x: beer.x + cap_size * 0.5,
    translate_y: beer.y + cap_size * 1.2,
    radar_radius: radar_width,
    radians: Math.PI,
    font_size: "22" / scale,
    explanation_label: "Smaakaanwezigheid",
    fill: true,
    color: beer_color,
    svg_rotation: 270,
    category_scale: 1.02,
    category_translate_y: -12,
  });
}

// function drawBeerAlcoholContent(beer, beer_color) : draws the beer style in svg left of the beer in the color of the beer
function drawBeerAlcoholContent(beer, beer_color) {
  Histogram.draw("vis" + beer.index, main_svg, abv_distribution, beer_abvs[beer.index], {
    unit: "%",
    type: "Gemeten alcohol",
    translate_x: beer.x - axis_width - cap_size * 0.10,
    translate_y: beer.y + cap_size * 0.2,
    height: axis_height,
    width: axis_width,
    max_value: 12,
    name: "abv_chart",
    svg_scale: scale,
    font_size: "20" / scale,
    focus_color: beer_color,
    ticks: 15
  });
}

// function drawBeerScore(beer, beer_color) : draws the beer style in svg right of the beer in the color of the beer
function drawBeerScore(beer, beer_color) {
  Histogram.draw("vis" + beer.index, main_svg, score_distribution, beer_scores[beer.index], {
    type: "Populariteit",
    translate_x: beer.x + cap_size * 1.10,
    translate_y: beer.y + cap_size * 0.2,
    height: axis_height,
    width: axis_width,
    max_value: 5,
    name: "score_chart",
    svg_scale: scale,
    font_size: "20" / scale,
    focus_color: beer_color
  });
}

// function removeBeerVisualisation(index) : remove all previous visualisations of one particular beer
export function removeBeerVisualisation(index) {
  d3.selectAll(".vis" + index)
    .remove();
}


const beer_color = [
  "#ffe699",
  "#ffe699",
  "#ffd978",
  "#ffca5a",
  "#ffca5a",
  "#ffbf43",
  "#fcb124",
  "#f8a700",
  "#f39c00",
  "#f39c00",
  "#ea8f00",
  "#e68500",
  "#de7c00",
  "#d77200",
  "#d77200",
  "#d06900",
  "#ca6200",
  "#c35901",
  "#bb5000",
  "#bb5000",
  "#b64c00",
  "#b04500",
  "#a63e00",
  "#a03700",
  "#a03700",
  "#9c3200",
  "#962d00",
  "#8f2a00",
  "#872300",
  "#872300",
  "#811e00",
  "#7c1902",
  "#771900",
  "#701400",
  "#701400",
  "#6a0e00",
  "#660d00",
  "#5f0b00",
  "#5a0a03",
  "#600902",
  "#600902",
  "#530908",
  "#4b0505",
  "#460606",
  "#440607",
  "#440607",
  "#3f0708",
  "#3a0608",
  "#3a080b",
  "#36080a",
  "#36080a",
  "#330000",
  "#300000",
  "#2c0001",
  "#290000",
  "#290000",
  "#260000",
  "#230000",
  "#220000",
  "#1e0000",
  "#1e0000",
  "#1c0000",
  "#1a0001",
  "#180000",
  "#160100",
  "#160100",
  "#140001",
  "#140001",
  "#120000",
  "#100000",
  "#100000",
  "#0e0000",
  "#0e0000"
];

// function determineBeerColor(ebc): determines the RGB color corresponding to the EBC color code
function determineBeerColor(ebc) {
  if (ebc > beer_color.length) {
    return "#0e0000";
  } else {
    return beer_color[ebc - 1];
  }
}