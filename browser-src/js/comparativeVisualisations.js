/*jshint esversion: 8 */

import { beer_abvs, beer_scores, beer_tastes, legend_color, scale, main_svg, cap_size, width, height, beer_cap_images } from "./visualisations.js";

// variables for comparison div
const comparison_svg = d3.select("#compare-svg");

const img_radius = 30;  // radius of the cap image
const max_comparison_barchart_height = img_radius * 6;
const radius_diff = 7;
const circle_radius = img_radius + radius_diff; // radius of the colored circle around the cap image
const div_rect = comparison_svg.node().getBoundingClientRect(); // total width of the div
const mod = Math.floor((div_rect.width - 10) / circle_radius / 2);  // max number of images next to each other in the div


// function updateComparativeVisualisation(beers) : update the full comparison view
export function updateComparativeVisualisation(beers) {

  removeComparativeVisualisation();  // remove previous visualizations
  const sliced_beers = beers.slice(0, 6); // visualise at most 6 beers

  // add colored border around each beer in the info view emphasizing the comparitive visualization colors
  drawComparisonRectangleLegend(sliced_beers);  // CHOICE: add rectangles always or only when there is a comparison

  if (sliced_beers.length > 1) // visualise at least 2 beers
  {
    const nb_of_beers_on_one_row = Math.min(mod, sliced_beers.length);
    const margin_left = Math.floor((div_rect.width - (circle_radius * nb_of_beers_on_one_row * 2)) / (nb_of_beers_on_one_row + 1));
    const margin_top = Math.min(margin_left, 10);
    const legend_height = margin_top + radius_diff + (img_radius * 2 + 10 + radius_diff * 2) * Math.ceil(sliced_beers.length / mod);

    const { abv_data, score_data, taste_data } = retrieveComparisonData(sliced_beers);  // extract the data for each beer that is compared

    drawComparisonCapLegend(sliced_beers, margin_left, margin_top);
    drawComparisonAlcoholContent(abv_data, legend_height);
    drawComparisonPopularity(score_data, legend_height);
    drawComparisonTaste(taste_data, legend_height);
  }
}

// function retrieveComparisonData(sliced_beers) : extract the datapoints of the beers that will be compared
function retrieveComparisonData(sliced_beers) {
  const abv_data = [];
  const score_data = [];
  const taste_data = [];
  sliced_beers.forEach(beer => {
    abv_data.push({ index: beer.index, value: beer_abvs[beer.index] });
    score_data.push({ index: beer.index, value: beer_scores[beer.index] });
    taste_data.push(beer_tastes[beer.index]);
  });
  return { abv_data, score_data, taste_data };
}

// function drawComparisonTaste(taste_data, legend_height) : draw the taste radarchart at the bottom of the comparison view
function drawComparisonTaste(taste_data, legend_height) {
  RadarChart.draw("compare", comparison_svg, taste_data, {
    max_value: 6,
    data_levels: 6,
    translate_x: div_rect.width / 2,
    translate_y: max_comparison_barchart_height * 2 + legend_height + 50 + (div_rect.width / 2.7),
    radar_radius: div_rect.width / 2.8,
    category_scale: 1.1,
    radians: 2 * Math.PI,
    stroke_width: "2px",
    color_scale: legend_color,
    font_size: "25" / scale,
    fill: false,
    svg_rotation: 0,
  });
  drawComparisonTasteTitle(legend_height);
}

// function drawComparisonTasteTitle(legend_height) : draw the taste radarchart title above the radarchart
function drawComparisonTasteTitle(legend_height) {
  comparison_svg
    .append("text")
    .text("Smaakaanwezigheid")
    .attr("id", "text-id")
    .attr("class", "text compare")
    .style("font-size", "28" / scale)
    .style("text-anchor", "middle")
    .attr("x", div_rect.width / 2)
    .attr("y", max_comparison_barchart_height * 2 + legend_height + 20);
}

// function drawComparisonPopularity(score_data, legend_height) : draw comparison barcharts of the beer popularity
function drawComparisonPopularity(score_data, legend_height) {
  BarChart.draw("compare", comparison_svg, score_data, {
    unit: "",
    type: "Populariteit op 5",
    color_scale: legend_color,
    max_value: 5,
    font_size: "28" / scale,
    name_translate_x: div_rect.width * 0.4,
    name_translate_y: -15,
    bar_width: div_rect.width * 0.7,
    translate_x: div_rect.width / 2 - (div_rect.width * 0.4),
    translate_y: max_comparison_barchart_height + legend_height + 20
  });
}

// function drawComparisonAlcoholContent(abv_data, legend_height) : draw comparison barcharts of the beer alcoholic content
function drawComparisonAlcoholContent(abv_data, legend_height) {
  BarChart.draw("compare", comparison_svg, abv_data, {
    unit: "%",
    type: "Gemeten alcohol",
    color_scale: legend_color,
    name_translate_x: div_rect.width * 0.4,
    max_value: 12,
    font_size: "28" / scale,
    name_translate_y: -15,
    bar_width: div_rect.width * 0.8,
    translate_x: div_rect.width / 2 - (div_rect.width * 0.4),
    translate_y: legend_height + 20
  });
}

// function drawComparisonRectangleLegend(sliced_beers) : draw the legend colors as a rectangle around each beer in the main view
function drawComparisonRectangleLegend(sliced_beers) {
  sliced_beers.forEach(function (beer, i) {
    main_svg
      .append("rect")
      .attr("x", beer.x + cap_size * 0.5 - width / 2)
      .attr("y", beer.y + cap_size * 0.5 - height / 3.2)
      .attr("width", width)
      .attr("height", height)
      .attr("class", "compare")
      .attr('fill', "white")
      .style("fill-opacity", 0)
      .attr('stroke', legend_color(i))
      .attr("opacity", 1)
      .attr("stroke-width", "2px")
      .attr("rx", 15)
      .attr("ry", 15);
  });
}

// function drawComparisonCapLegend(sliced_beers, margin_left, margin_top) : draw the legend colors with the beercap images
function drawComparisonCapLegend(sliced_beers, margin_left, margin_top) {
  sliced_beers.forEach(function (beer, i) {
    // add a colored circle around each beer cap image which emphasizing the comparitive visualization colors
    comparison_svg
      .append("svg:circle")
      .attr("r", circle_radius)
      .attr("cx", margin_left + circle_radius + (circle_radius * 2 + margin_left) * (i % mod))
      .attr("cy", margin_top + circle_radius + (circle_radius * 2 + 10) * Math.floor(i / mod))
      .style("fill", "none")
      .style("opacity", 1)
      .style('stroke', legend_color(i))
      .style('stroke-width', 4)
      .attr("class", "compare")
      .style("opacity", 0.6);
    // add the beer cap image as a legend
    comparison_svg
      .append("svg:image")
      .attr("xlink:href", beer_cap_images[beer.index])
      .attr("class", "img compare")
      .attr("width", img_radius * 2)
      .attr("height", img_radius * 2)
      .attr("x", margin_left + radius_diff + (img_radius * 2 + margin_left + radius_diff * 2) * (i % mod))
      .attr("y", margin_top + radius_diff + (img_radius * 2 + 10 + radius_diff * 2) * Math.floor(i / mod));
  });
}

// function removeComparativeVisualisation() : remove all comparative visualisations
function removeComparativeVisualisation() {
  d3
    .selectAll(".compare")
    .remove();
}