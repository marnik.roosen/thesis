/*jshint esversion: 8 */

// divs in which the visualizations will be shown
export const main_svg = d3.select("#vis-svg");

// data holders of csv file
export const beer_names = [];
export const beer_tastes = [];
export const beer_styles = [];
export const beer_abvs = [];
export const beer_colors = [];
export const beer_scores = [];
export const beer_cap_images = [];
export const abv_distribution = [];
export const score_distribution = [];
export const legend_color = d3.scaleOrdinal().domain([0, 1, 2, 3, 4, 5])
  .range(['#377eb8', '#e41a1c', '#4daf4a', '#ff7f00', '#984ea3', "#b15928"]);
const all_beer_styles = [];

// initializer for the scale of the drawings
export const scale = 1.4;
export const width = 600 / scale;
export const height = width * 1.08;
export const cap_size = width / 3;


initializeData(); // Main function call of this file which initializes data extraction from csv files


// function initializeData(): initialise the data from the csv files
async function initializeData() {
  await initializeBeerStyles();
  initializeBeerData();
  initializeBeerDistributions();
}

// function initializeBeerStyles(): initialize beerstyles from the styles.csv files
async function initializeBeerStyles() {
  d3.csv("/data/styles.csv")
    .then(function (data) {
      data.forEach(function (row) {
        all_beer_styles.push(row.style_name);
      });
    })
    .catch(error => console.error("An error occured during initialization of data from the styles.csv file."));
  return;
}

// function initializeBeerData(): initialize general beer data from the beers.csv files
async function initializeBeerData() {
  d3
    .csv("/data/beers.csv")
    .then(function (data) {
      data.forEach(function (row, i) {
        visualizeBeerImages(row.image, i);
        beer_cap_images.push("/images/display_images/" + row.image + "-cap-small.png");
        beer_names.push(row.beer_name);
        beer_styles.push({ id: i, style_id: +row.style_id, value: all_beer_styles[+row.style_id] });
        beer_abvs.push(+row.abv);
        beer_colors.push(+row.ebc);
        beer_scores.push(+row.score);
        beer_tastes.push(getBeerTaste(row));
      });
    })
    .catch(error => console.error("An error occured during initialization of data from the beers.csv file."));
}

// function initializeBeerDistributions(): initialize distributions for abv and scores from the distribution.csv files
function initializeBeerDistributions() {
  d3
    .csv("/data/distribution.csv")
    .then(function (data) {
      data.forEach(row => {
        score_distribution.push(row.review_overall);
        abv_distribution.push(row.beer_abv);
      });
    })
    .catch(error => console.error("An error occured during initialization of data from the distribution.csv file."));
}

// function getBeerTaste(row): get the taste attributes from a row of beers.csv
function getBeerTaste(row) {
  var j = 0;
  var temp = [];
  const start_index_of_taste = 9; // taste data starts at the 9th column
  Object.values(row)
    .splice(start_index_of_taste, start_index_of_taste + 6)
    .forEach(function (val) {
      temp.push({ axis: Object.keys(row)[j + start_index_of_taste], value: val });
      j++;
    });
  return temp;
}

// function visualizeBeerImages(image, i): get the taste attributes from a row of beers.csv
function visualizeBeerImages(image, i) {

  if (image !== "") {
    var pic = d3
      .select("#bottles")
      .append("picture");
    pic.append("source")
      .attr("type", "image/webp")
      .attr("srcset", "/images/display_images/" + image + "-bottle-small.webp");
    pic.append("source")
      .attr("type", "image/webp")
      .attr("srcset", "/images/display_images/" + image + "-bottle-small.png");
    pic.append("img")
      .attr("src", "/images/display_images/" + image + "-bottle-small.png")
      .attr("class", "draggable img")
      .attr("id", "drag" + i)
      .attr("title", i)
      .attr("width", cap_size)
      .attr("height", cap_size)
      .attr("in-dropzone", 0)
      .attr("clonable", "true");
  }
}