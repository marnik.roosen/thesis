/*jshint esversion: 6 */

import { removeBeerVisualisation, updateBeerVisualisation } from "./mainBeerVisualisations.js";
import { updateComparativeVisualisation } from "./comparativeVisualisations.js";


const dragPositions = [...Array(29).keys()].reduce((acc, n) => {
  acc[`clone${n}`] = { x: 0, y: 0 };
  return acc;
}, {});
var visualised_beers = [];


setupDropzoneInteraction();
setupDraggableElementEventListeners();


function setupDraggableElementEventListeners() {
  interact('.draggable') // setup interactions for elements with draggable classname
    .draggable({
      listeners: {
        start(event) {
          startMovementOfDraggableElement(event); // handle start of movement
        },
        move(event) {
          moveDraggableElementEvent(event); // handle held-down movement
        },
        end(event) {
          endMovementOfDraggableElement(event); // handle end of movement
        },
      },
    });
}

function setupDropzoneInteraction() {
  interact('.dropzone') // setup interaction for elements with dropzone classname
    .dropzone({
      accept: '.draggable', // accept elements with draggable classname
      overlap: 1, // element must fully lie in side the dropzone to be accepted
    })
    .on('dragenter', (event) => {

      event.relatedTarget.setAttribute('in-dropzone', 1); // set in-dropzone attribute when it is dragged in the dropzone
    })
    .on('dragleave', (event) => {
      event.relatedTarget.setAttribute('in-dropzone', 0); // clear in-dropzone attribute when it is dragged out the dropzone
    })
    .on('dropactivate', (event) => {
      event.target.style.border = "1px dashed";  // update dropzone when element is dragged on the dropzone
    })
    .on('dropdeactivate', (event) => {
      event.target.style.border = "1px solid"; // update dropzone when element is dragged away from the dropzone
    })

    .on('drop', (event) => {
      visualizeBeerInformation(event); // draw visualizations when the element is dropped in the dropzone
    });

}

function startMovementOfDraggableElement(event) {

  // remove the visualization of the element at the start of the movement
  removeVisualizationOfElement(event);

  // clone once when starting to drag from the starting position (to get it out of the scrollable div)
  if (event.target.getAttribute('in-dropzone') == 0 && event.target.getAttribute('clonable') == "true") {
    propagateDragInteractionToClone(event);
  }
  else {
    // set the dragPosition to the startposition of element
    const position = dragPositions[event.target.id];
    position.x = parseInt(event.target.style.left, 0);
    position.y = parseInt(event.target.style.top, 0);
  }
}

function removeVisualizationOfElement(event) {
  visualised_beers = visualised_beers.filter(beer => beer.index !== event.target.title);
  removeBeerVisualisation(event.target.title);
  updateComparativeVisualisation(visualised_beers);
}

// beer images are part of a scrollable div, this makes it so that the element can't get dragged out of it
//  => clone the element and interact with the clone (which can get out of the scrollable div)
function propagateDragInteractionToClone(event) {

  // hide the original element & clone the element (only once)
  var original_element = event.currentTarget;
  var original_rect = original_element.getBoundingClientRect();
  var cloned_element = event.currentTarget.cloneNode(true);
  original_element.style.visibility = "hidden";

  // set attributes to interact with the clone
  cloned_element.setAttribute('clonable', "false");
  cloned_element.setAttribute('id', "clone" + original_element.getAttribute("title"));
  cloned_element.setAttribute('data-x', original_rect.x);
  cloned_element.setAttribute('data-y', original_rect.y);

  // set the clone to the same position as the original (as if its the same element)
  cloned_element.style.position = "absolute";
  cloned_element.style.left = original_rect.x + "px";
  cloned_element.style.top = original_rect.y + "px";
  original_element.parentNode.appendChild(cloned_element);

  // continue drag interaction with the clone
  event.interaction.start({ name: 'drag' }, event.interactable, cloned_element);
}

function moveDraggableElementEvent(event) {
  // increment previous dragPosition with displacement
  const position = dragPositions[event.target.id];
  position.x += event.dx;
  position.y += event.dy;
  event.target.style.left = position.x + 'px';
  event.target.style.top = position.y + 'px';
}

function endMovementOfDraggableElement(event) {

  // element not in dropzone: delete the clone and unhide the original
  if (event.target.getAttribute('in-dropzone') == 0) {
    var original = document.getElementById("drag" + event.target.getAttribute("title"));
    original.style.visibility = "visible";
    var clone = document.getElementById(event.target.getAttribute("id"));
    clone.parentNode.removeChild(clone);
  }
}

function visualizeBeerInformation(event) {

  // beer location and id
  const beer_tuple = {
    x: (event.relatedTarget.x / window.devicePixelRatio - event.target.getBoundingClientRect().left),
    y: (event.relatedTarget.y / window.devicePixelRatio - event.target.getBoundingClientRect().top),
    index: event.relatedTarget.title
  };

  // add index to list of visualised beers
  visualised_beers.push(beer_tuple);

  updateBeerVisualisation(beer_tuple, event.relatedTarget.title);
  updateComparativeVisualisation(visualised_beers);
}