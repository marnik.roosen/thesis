# Thesis: Interactieve visualisaties om consumenten te helpen bij het kiezen van producten in een supermarkt

## Mappen
- **browser-src**:          webbrowser implementatie van het systeem
- **cameraprojector-src**:  camera-projector implementatie van het systeem
- **data**:                 bierinformatie in csv bestanden
- **images**:               vergelijkende afbeeldingen voor de classificatie van flesjes + webbrowser afbeeldingen van flesjes
- **libs**:                 opencv.js library + zelf geimplementeerde visualisaties in D3.js
- **latex**:               geschreven thesis in latex

## Ontwerp

# Webbrowser ontwerp
De webbrowser implementatie is volledig afgerond en ziet er zo uit:
![alt text](./latex/afbeeldingen/hfdst-5/finaal-ontwerp.PNG)

# Camera-projector ontwerp
Het camera-projector ontwerp zou er zo moeten uitzien:
![alt text](./latex/afbeeldingen/hfdst-1/front.jpg)

Door de Covid-19 maatregelen werd de implementatie aangepast naar het webbrowser ontwerp.
De camera-projector implementatie kon hierdoor niet volledig worden afgewerkt.
Er is nog wat werk aan de classificatiemethode om deze implementatie te kunnen gebruiken.