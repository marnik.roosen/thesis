// updateVisualisations()
function updateVisualisations() {
  // remove previous visualisations
  svg.selectAll("#text-id")
    .remove();
  svg.selectAll(".radarchart")
    .remove();
  svg.selectAll(".barchart")
    .remove();
  visualized_bottles.forEach(function (beer, i) {
    drawBeerName(svg, beer);
    drawBeerStyle(svg, beer);
    const beer_color = determineBeerColor(ebc[beer.index]);
    drawBeerTaste(beer, beer_color);
    drawAlcoholContent(beer, beer_color);
    drawBeerScore(beer, beer_color);
  });
}

function drawBeerName(svg, beer) {
  svg
    .append("text")
    .text(beer_name[beer.index])
    .attr("id", "text-id")
    .attr("class", "text" + " " + "vis" + beer.index)
    .style("font-size", "40" / scale)
    .style("text-anchor", "middle")
    .attr("x", beer.x)
    .attr("y", beer.y - cap_size * 0.7);
}

function drawBeerStyle(svg, beer) {
  svg
    .append("text")
    .text(beer_style[beer.index].value)
    .attr("id", "text-id")
    .attr("class", "text" + " " + "vis" + beer.index)
    .style("text-anchor", "middle")
    .style("font-size", "25" / scale)
    .attr("x", beer.x)
    .attr("y", beer.y - cap_size * 0.4);
}

function drawBeerTaste(beer, beer_color) {
  RadarChart.draw("vis" + beer.index, svg, [beer_taste[beer.index]], {
    max_value: 6,
    data_levels: 6,
    translate_x: beer.x,
    translate_y: beer.y + cap_size * 1,
    radar_radius: radar_width,
    radians: Math.PI,
    font_size: "22" / scale,
    explanation_label: "Smaakaanwezigheid",
    fill: true,
    color: beer_color,
    svg_rotation: 270,
    category_scale: 1.02,
    category_translate_y: -12,
  });
}

function drawAlcoholContent(beer, beer_color) {
  Histogram.draw("vis" + beer.index, svg, abv_distribution, abv[beer.index], {
    unit: "%",
    type: "Gemeten alcohol",
    translate_x: beer.x - axis_width - cap_size * 0.70,
    translate_y: beer.y - cap_size * 0.20,
    name_translate_x: 0,
    name_translate_y: -5,
    height: axis_height,
    width: axis_width,
    max_value: 12,
    name: "abv_chart",
    svg_scale: scale,
    font_size: "20" / scale,
    focus_color: beer_color,
    ticks: 15
  });
}

function drawBeerScore(beer, beer_color) {
  Histogram.draw("vis" + beer.index, svg, score_distribution, score[beer.index], {
    type: "Populariteit",
    translate_x: beer.x + cap_size * 0.70,
    translate_y: beer.y - cap_size * 0.20,
    name_translate_y: -5,
    height: axis_height,
    width: axis_width,
    max_value: 5,
    name: "score_chart",
    svg_scale: scale,
    font_size: "20" / scale,
    focus_color: beer_color
  });
}
