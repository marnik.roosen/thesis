


// load the opencv library (takes a while)
function onCvLoaded() {
  console.log('cv', cv);
  cv.onRuntimeInitialized = onOpenCVReady;
}

// videostream constants
const video = document.getElementById('video');
const generateCapTemplate = false;
if (!generateCapTemplate) {
  document.getElementById('capOutput').setAttribute("hidden", "true");
}
const width = 1920;
const height = 1080;
const FPS = 30;
const scale = 1;
const text_scale = 1;
var visualized_bottles = [];
var svg;
var colorscale;

let src;
let transformation_values;
let previous;
let cap;

const beer_color = [
  "#ffe699",
  "#ffe699",
  "#ffd978",
  "#ffca5a",
  "#ffca5a",
  "#ffbf43",
  "#fcb124",
  "#f8a700",
  "#f39c00",
  "#f39c00",
  "#ea8f00",
  "#e68500",
  "#de7c00",
  "#d77200",
  "#d77200",
  "#d06900",
  "#ca6200",
  "#c35901",
  "#bb5000",
  "#bb5000",
  "#b64c00",
  "#b04500",
  "#a63e00",
  "#a03700",
  "#a03700",
  "#9c3200",
  "#962d00",
  "#8f2a00",
  "#872300",
  "#872300",
  "#811e00",
  "#7c1902",
  "#771900",
  "#701400",
  "#701400",
  "#6a0e00",
  "#660d00",
  "#5f0b00",
  "#5a0a03",
  "#600902",
  "#600902",
  "#530908",
  "#4b0505",
  "#460606",
  "#440607",
  "#440607",
  "#3f0708",
  "#3a0608",
  "#3a080b",
  "#36080a",
  "#36080a",
  "#330000",
  "#300000",
  "#2c0001",
  "#290000",
  "#290000",
  "#260000",
  "#230000",
  "#220000",
  "#1e0000",
  "#1e0000",
  "#1c0000",
  "#1a0001",
  "#180000",
  "#160100",
  "#160100",
  "#140001",
  "#140001",
  "#120000",
  "#100000",
  "#100000",
  "#0e0000",
  "#0e0000"
];

// function determineBeerColor(ebc): determines the RGB color corresponding to the EBC color code
function determineBeerColor(ebc) {
  if (ebc > beer_color.length) {
    return "#0e0000";
  } else {
    return beer_color[ebc - 1];
  }
}

// data holders of csv file
const beerinfo_div = "#beer-svg";
const beer_name = [];
const beer_taste = [];
const beer_style = [];
const abv = [];
const abv_distribution = [];
const ibu = [];
const ebc = [];
const score = [];
const score_distribution = [];
const beer_varia = [];
const all_beer_styles = [];
const myColor = d3.scaleOrdinal().domain([0, 1, 2, 3, 4, 5])
  .range(['#377eb8', '#e41a1c', '#ff7f00', '#4daf4a', '#984ea3', "#b15928"]);

// variables which get adjusted using the scale
// initializer for the scale of the drawings
var ascale = 1;

// variables which get adjusted using the scale
var awidth = 600 / ascale;
var aheight = awidth * 1.08;
const cap_size = awidth / 3;
const axis_width = awidth / 4;
const axis_height = aheight / 5;
const radar_width = awidth * 0.43;


// initialise the data from the csv files and setup a svg of full width-height dimensions
initializeD3();

// setup the videostream
function onOpenCVReady() {

  // setup the videostream
  cap = new cv.VideoCapture(video);
  navigator.mediaDevices.getUserMedia({
    video: {
      width: { ideal: width },
      height: { ideal: height }
    }, audio: false
  })
    .then(_stream => {
      video.srcObject = _stream;
      video.play();
      src = new cv.Mat(height, width, cv.CV_8UC4);
      caps = new cv.Mat(height, width, cv.CV_8UC4);
      previous = new cv.Mat(height, width, cv.CV_8UC4);
      cap.read(previous);
      setTimeout(processVideo, 50)
    })
    .catch(err => console.log(`An error occurred during the videostream setup: ${err}`));
}

// processVideo(): process the video source
function processVideo() {
  const begin = Date.now();
  cap.read(src);

  var norm_diff = cv.norm1(previous, src);
  if (norm_diff < 10000) {

    if (transformation_values == null) {
      determinePerspectiveTransformation();
    }
    else {
      extractAndClassifyBottleCaps();
    }
    updateVisualisations();
  }

  // wait a bit before processing the next image
  cap.read(previous);
  const delay = 3000 / FPS - (Date.now() - begin);  // 100ms
  setTimeout(processVideo, delay);

}

function initializeD3() {

  // make one big svg of full dimensions in which all other visualisations will be drawn
  svg = d3
    .select("#beer-app")
    .append("svg")
    .attr("id", "beer-svg")
    .attr("width", '100%')
    .attr("height", '100%');

  // initialise all data from beers.csv file
  initialiseBeerData();

  async function initialiseBeerData() {

    // initialise beer styles from style.csv file
    await initializeBeerStyles();

    d3
      .csv("/data/beers.csv")
      .then(function (data) {
        data.forEach(function (row, i) {
          beer_name.push(row.beer_name);
          beer_style.push({ id: i, style_id: +row.style_id, value: all_beer_styles[+row.style_id] });
          beer_varia.push(row.varia);
          abv.push(+row.abv);
          ibu.push(+row.ibu);
          ebc.push(+row.ebc);
          score.push(+row.score);
          // beer taste
          var i = 0;
          var temp = [];
          const start_index_of_taste = 9;
          Object.values(row)
            .splice(start_index_of_taste, start_index_of_taste + 6) // 
            .forEach(function (val) {
              temp.push({ axis: Object.keys(row)[i + start_index_of_taste], value: val });
              i++;
            });
          beer_taste.push(temp);
        });
      })
      .catch(function (error) {
        console.log("An error occured during initialization of data from the csv file.");
        console.log(error);
      });

    d3
      .csv("/data/distribution.csv")
      .then(function (data) {
        data.forEach(function (row, i) {
          score_distribution.push(row.review_overall);
          abv_distribution.push(row.beer_abv);
        });
      })
      .catch(function (error) {
        console.log("An error occured during initialization of data from the csv file.");
        console.log(error);
      });
  }

  async function initializeBeerStyles() {
    d3.csv("/data/styles.csv")
      .then(function (data) {
        data.forEach(function (row) {
          all_beer_styles.push(row.style_name);
        });
      });
    return;
  }
}



