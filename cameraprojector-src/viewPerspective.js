// determinePerspectiveTransformation(): determine the perspective transformation to extract the projector image
function determinePerspectiveTransformation() {
  try {
    // rotate the image 180° (the camera captures the image upside-down)
    cv.rotate(src, src, cv.ROTATE_180);
    // use a smaller image file for processing
    let intermediate = new cv.Mat();
    // threshold the image to extract the projected image
    cv.cvtColor(src, intermediate, cv.COLOR_RGBA2GRAY);
    cv.threshold(intermediate, intermediate, 140, 255, cv.THRESH_BINARY);
    // find all contours in the image
    let contours = new cv.MatVector();
    let hierarchy = new cv.Mat();
    cv.findContours(intermediate, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);
    // find the contour of the projected image
    let area;
    for (let i = 0; i < contours.size(); ++i) {
      area = cv.contourArea(contours.get(i), false);
      // resized columns and rows so divide twice
      if (area > 1920 * 1080 / 2) {
        // find an approximate rectangle that encloses the full projection
        let projection = new cv.Mat();
        cv.approxPolyDP(contours.get(i), projection, Math.floor(500), true);
        // extract the coordinates from the rectangle
        let pts = [];
        for (let i = 0; i < projection.rows; i++) {
          pts.push({ x: projection.data32S[i * 2], y: projection.data32S[i * 2 + 1], sum: projection.data32S[i * 2] + projection.data32S[i * 2 + 1], diff: projection.data32S[i * 2] - projection.data32S[i * 2 + 1] });
        }
        projection.delete();
        // determining the outer coordinates of the rectangle
        //    https://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/
        let bottom_right = pts.reduce(function (prev, current) {
          return (prev.sum > current.sum) ? prev : current;
        });
        let top_left = pts.reduce(function (prev, current) {
          return (prev.sum < current.sum) ? prev : current;
        });
        let top_right = pts.reduce(function (prev, current) {
          return (prev.diff > current.diff) ? prev : current;
        });
        let bottom_left = pts.reduce(function (prev, current) {
          return (prev.diff < current.diff) ? prev : current;
        });
        // perform a perspective transformation to get the projection as a regular sized image
        let srcTri = cv.matFromArray(4, 1, cv.CV_32FC2, [top_left.x, top_left.y, top_right.x, top_right.y, bottom_right.x, bottom_right.y, bottom_left.x, bottom_left.y]);
        let dstTri = cv.matFromArray(4, 1, cv.CV_32FC2, [0, 0, width - 1, 0, width - 1, height - 1, 0, height - 1]);
        transformation_values = cv.getPerspectiveTransform(srcTri, dstTri);
        cv.warpPerspective(src, src, transformation_values, new cv.Size(width, height), cv.INTER_LINEAR, cv.BORDER_CONSTANT);
        // delete all remaining image references
        srcTri.delete();
        dstTri.delete();
      }
    }
    intermediate.delete();
    contours.delete();
    hierarchy.delete();
  }
  catch (err) {
    console.log(cv.exceptionFromPtr(err).msg);
  }
}
