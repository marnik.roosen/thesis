// extractAndClassifyBottleCaps(): extracting and classify the bottle caps + store the coordinates
//  1. rotate the image and change the perspective to account for the cameraview
//  2. convert to greyscale and perform a medianblur (which keeps edges clear)
//  3. canny edge detection with a high threshold to find only clear edges
//  4. hough circles detection: 
//      this method isn't perfect for white caps, so we will extract a big region around the circle
//  5. find contours for each circle:
//      this method can determine the exact region of the bottle cap much better
//      (if we do this before hough circles, we would detect many non-circle contours)
//  6. extract the largest contour and classify using existing cap images
function extractAndClassifyBottleCaps() {
  try {
    // rotate the image 180° (the camera captures the image upside-down)
    cv.rotate(src, src, cv.ROTATE_180);
    cv.warpPerspective(src, src, transformation_values, new cv.Size(width, height), cv.INTER_LINEAR, cv.BORDER_CONSTANT);

    var circles = findCirclesInImage(src);

    // extract each circle for classification and save the new coordinates
    visualized_bottles = [];
    for (let i = 0; i < circles.cols; ++i) {

      var bottle_cap;
      var region;
      var rect;

      ({ bottle_cap, region, rect } = segmentBottleRegion(circles, i));


      var comparison = new cv.MatVector;
      const bottles = ["duvel", "stella"];
      const values = [{ index: 0, text: "Duvel" }, { index: 1, text: "Stella" }];
      bottles.forEach(function (bottle_name) {
        var bottle = cv.imread(bottle_name);
        comparison.push_back(bottle);
      });


      // if we want to generate a template for a new bottle cap
      if (generateCapTemplate) {
        cv.imshow('capOutput', bottle_cap);
      }
      else {
        classifyBottle(region, rect, comparison, bottle_cap, values);
      }

      // delete mats to avoid a memory leak
      for (let i = 0; i < comparison.size(); i++) {
        comparison.get(i).delete();
      }
      comparison.delete();
      bottle_cap.delete();
    }
    circles.delete();
  }
  catch (err) {
    console.log(cv.exceptionFromPtr(err).msg);
  }
}


function classifyBottle(region, rect, comparison, bottle_cap, values) {
  var norm = 1000000;
  var new_elem = {
    x: (region.x + rect.x + (rect.width / 2)),
    y: (region.y + rect.y + (rect.height / 2)),
    index: -1,
    text: ""
  };
  if (!visualized_bottles.some(elem => Math.abs(elem.x - new_elem.x) < 30 && Math.abs(elem.y - new_elem.y) < 30)) {
    for (let i = 0; i < comparison.size(); i++) {
      let iter_norm = cv.norm1(comparison.get(i), bottle_cap);
      if (iter_norm < norm) {
        new_elem.index = values[i].index;
        new_elem.text = values[i].text;
        norm = iter_norm;
      }
    }
    visualized_bottles.push(new_elem);
  }
}

// function segmentBottleRegion(circles, index)
//  1. extract a general region around the bottle cap (to remove noise)
//  2. use contours to find the largest contour in that region (= bottle cap)
function segmentBottleRegion(circles, index) {
  var bottle_cap_region = new cv.Mat();
  var grey_temp = new cv.Mat();
  var temp_contours = new cv.MatVector();
  var temp_hierarchy = new cv.Mat();
  var bottle_cap = new cv.Mat();

  // extract the region around the bottle cap
  var pt = new cv.Point(
    Math.floor(circles.data32F[index * 3]), // x
    Math.floor(circles.data32F[index * 3 + 1])); // y
  var radius = Math.floor(circles.data32F[index * 3 + 2] * 1.35); // r
  var region = new cv.Rect(pt.x - radius, pt.y - radius, 2 * radius, 2 * radius);
  bottle_cap_region = src.roi(region);


  // find the region of the largest contour in the general region
  cv.cvtColor(bottle_cap_region, grey_temp, cv.COLOR_RGBA2GRAY);
  cv.threshold(grey_temp, grey_temp, 210, 255, cv.THRESH_BINARY_INV);
  cv.findContours(grey_temp, temp_contours, temp_hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);
  var temp_area = 0;
  var index = 0;
  for (let j = 0; j < temp_contours.size(); ++j) {
    let iter_area = cv.contourArea(temp_contours.get(j), false);
    if (iter_area > temp_area && iter_area < grey_temp.rows * grey_temp.cols * 0.95) {
      index = j;
      temp_area = iter_area;
    }
  }
  var rect = cv.boundingRect(temp_contours.get(index));
  bottle_cap = bottle_cap_region.roi(rect);

  // resize the bottle cap for stable image comparison
  cv.resize(bottle_cap, bottle_cap, new cv.Size(100, 100), 0, 0, cv.INTER_AREA);

  bottle_cap_region.delete();
  temp_contours.delete();
  temp_hierarchy.delete();
  grey_temp.delete();

  return { bottle_cap, region, rect };
}

function findCirclesInImage(src) {
  var intermediate = new cv.Mat();

  // perform a medium blur to get better circles
  cv.cvtColor(src, intermediate, cv.COLOR_RGBA2GRAY);
  cv.medianBlur(intermediate, intermediate, 9);

  // canny edge detection with high thresholds to get only good edges
  cv.Canny(intermediate, intermediate, 500, 100, 3, false);
  cv.blur(intermediate, intermediate, new cv.Size(3, 3), new cv.Point(-1, -1), cv.BORDER_DEFAULT);

  // hough circles to find circles of the correct size
  var circles = new cv.Mat();
  cv.HoughCircles(intermediate, circles, cv.HOUGH_GRADIENT, 1, Math.floor(height) * 0.03, 500, 20, Math.floor(height) * 0.03, Math.floor(height) * 0.07);

  intermediate.delete();

  return circles;
}

