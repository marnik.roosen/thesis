\chapter{Methodologie}
\label{chapter:methodologie}

Om onderzoeksvragen \ref{RQ1} en \ref{RQ2} te beantwoorden, ontwikkelen we een systeem dat interactieve visualisaties rond producten toont. Dit systeem moet consumenten informeren over verschillende producten. Tijdens de finale evaluatie zullen gebruikers het systeem testen om na te gaan indien ze zekerder zijn over hun keuzes en indien ze de productinformatie beter begrijpen. In dit deel bespreken we in de eerste plaats hoe het systeem wordt ontwikkeld. Vervolgens bespreken we de opzet van de finale evaluatie.

\section{Ontwikkelingsproces}
\label{sectie:methodologie:ontwikkelingsproces}

In dit onderzoek wordt een systeem ontwikkeld om informatie rond producten te kunnen visualiseren met een camera-projector opstelling. Dit systeem zal worden ontwikkeld via een iteratief, gebruikersgericht ontwerp. Hierbij worden iteraties van prototypes getest met gebruikers om ervoor te zorgen dat het ontwerp voldoet aan de behoeftes van gebruikers. Op deze manier worden gebruikers dicht bij de ontwikkeling van het systeem betrokken.

Een iteratief proces is redelijk tijdsintensief. Een goede afweging is nodig van het aantal testgebruikers. Bij te weinig testen kunnen bepaalde problemen in het ontwerp niet worden waargenomen. Bij te veel testen wordt het ontwerp te veel afgestemd op de specifieke testgebruikers \cite{abras2004user}. Daarom kiezen we om het aantal testgebruikers te laten vari\"eren afhankelijk van het doel van de iteratiestap.

Als startpunt wordt het systeem ontwikkeld voor de \textit{casestudy} van bieren zoals besproken in Hoofdstuk \ref{chapter:inleiding}. In Sectie \ref{sectie:literatuurstudie:bieren:literatuur} werden reeds biereigenschappen besproken die gebruikt kunnen worden als startpunt voor het systeem. Het iteratief ontwerp helpt bij het controleren welke productinformatie essentieel is voor gebruikers.  Deze aanpak komt overeen met gelijkaardige studies in het informatie visualisatie domein \cite{decroon,GutierrezHernandezFrancisco2017Paph}. 

\newpage

De prototypes werden ge\"itereerd over vier deelstudies. Het concept van het systeem wordt gemaakt als een \textit{Low Fidelity} papieren prototype. Dit maakt het mogelijk om het concept in een vroege fase te testen met zonder technologische inspanning. Met behulp van de feedback kan het concept ge\"itereerd worden tot een \textit{High Fidelity} digitaal prototype. Dit prototype staat dichter bij het finale ontwerp en vereist technologische inspanning.

\subsection{Concept}

Inspiratie voor de papieren prototypes wordt gehaald bij bestaande systemen in de literatuur. Hiervan worden verschillende prototypes gemaakt. In deze eerste deelstudie zullen gebruikers met al deze prototypes interageren. Hierdoor proberen we uit te zoeken welk prototype het meest geschikt is om verder mee te werken. Dit prototype moet intu\"itieve interactiemogelijkheden hebben voor gebruikers. Bovendien moet informatie duidelijk worden voorgesteld. Dit prototype zal een basis vormen voor het \textit{Low Fidelity} digitaal prototype. 

De papieren prototypes worden ge\"evalueerd via een \textit{think-aloud} studie \cite{Nielsen1994} gevolgd door een half-gestructureerd interview. Deelnemers krijgen concrete taken die ze moeten uitvoeren. Tijdens het uitvoeren denken ze hardop waar ze naar kijken, wat ze verwachten enzovoort. De onderzoekers observeren het gedrag en nemen notities. Mogelijke taken zijn het zoeken van bepaalde informatie of het uitvoeren van een concrete functionaliteit. Appendix \ref{app:smec} bevat een uitgebreide lijst met concrete taken die gesteld zijn.

Tijdens het half-gestructureerde interview worden open vragen gesteld zodat de deelnemers hun mening kunnen geven. Mogelijke vragen zijn het ontbreken van bepaalde informatie of functionaliteit, het veranderen van bepaalde aspecten van het ontwerp enzovoort. Ook hier verwijzen we naar Appendix \ref{app:smec} voor meer details.

Deze iteratiestap dient voornamelijk om kwalitatieve data te verkrijgen. We kozen ervoor om de prototypes te evalueren in een testgroep van vijf personen. Dit aantal valt binnen het aangeraden bereik voor het verkrijgen van kwalitatieve data \cite{dworkin2012sample}. De voorwaarde voor deze testgroep is dat de deelnemers wel eens een bier kopen in de supermarkt. Deelnemers die nooit een bier kopen zullen het prototype ook niet gebruiken indien het in een supermarkt zou staan. Het verloop van deze iteratiestap wordt besproken in Sectie \ref{sectie:iteratiefontwerp:1concept}.

\subsection{Gebruiksvriendelijkheid}

De \textit{feedback} van de eerste deelstudie resulteert in een \textit{High Fidelity} digitaal prototype. Dit ontwerp bevat reeds de informatie die in het finale ontwerp zal worden getoond. Tijdens de tweede deelstudie wordt dit digitale ontwerp beoordeelt door een HCI-expert. Het doel van deze evaluatie is om de gebruiksvriendelijkheid na te gaan en veelvoorkomende \textit{usability issues} vroeg te kunnen vaststellen. Deze evaluatie gebeurt via een ongestructureerd interview waarbij het digitale ontwerp wordt gediscussieerd. De resultaten van deze iteratiestap zijn beschreven in Sectie \ref{sectie:iteratiefontwerp:2gebruiksvriendelijkheid}.

\subsection{Intermediaire studie}

De derde deelstudie vormt een intermediaire studie voor de finale evaluatie. Hier worden zowel het \textit{High Fidelity} digitaal prototype en de smartphone applicatie Untappd ge\"evalueerd op dezelfde manier. De reden voor de vergelijkende studie en vergelijking met Untappd wordt in detail besproken in Sectie \ref{sectie:methodologie:finale-evaluatie:vergelijkende-studie}. Door Untappd mee te evalueren krijgen we een inzicht in de moeilijkheden die deelnemers hebben bij het gebruik. Deze informatie kan tijdens de finale evaluatie gebruikt worden om extra uitleg te geven. Dit zorgt ervoor dat de vergelijking dan eerlijker zal verlopen.

De deelstudie begint met een soortgelijk protocol aan de eerste deelstudie. Deelnemers zullen via een \textit{think-aloud} studie het digitale prototype en Untappd gebruiken door taken uit te voeren. Vervolgens vullen de deelnemers meerdere vragenlijsten in. De eerste twee vragenlijsten zijn identiek aan de vragen van de finale evaluatie, namelijk demografie en de \textit{System Usability Scale}. De redenering voor deze vragen wordt in detail uitgelegd in Sectie \ref{sectie:methodologie:finale-evaluatie:protocol}. De laatste vragenlijst bevat enkele concrete vragen over de interface, welke informatie getoond wordt, hoe de informatie getoond wordt enzovoort. De volledige vragenlijst is zichtbaar in Appendix \ref{app:smec}.

Deze deelstudie wordt gebruikt om meerdere aspecten van de systemen te evalueren. In de eerste plaats worden de individuele visualisaties rond producten ge\"evalueerd. Daarnaast wordt de interactie met het systeem getest. De vragenlijsten worden gebruikt om de gebruiksvriendelijkheid te testen en een algemeen beeld van de systemen te vormen. We kiezen om de deelstudie uit te voeren met tien deelnemers om voldoende feedback te krijgen van al deze aspecten. De resultaten van deze deelstudie zijn beschreven in Sectie \ref{sectie:iteratiefontwerp:intermediair}.

\subsection{Validatie vernieuwd ontwerp}

Uit de derde deelstudie zal veel informatie gehaald kunnen worden. Hiermee zal het digitale prototype verbeterd worden tot een systeem dat dicht bij het finale ontwerp ligt. Een laatste deelstudie wordt uitgevoerd om de aanpassingen te evalueren. De data uit deze deelstudie zal gebruikt worden om kleine verbeteringen uit te voeren om tot het finale ontwerp te komen.

De aanpassingen worden ge\"evalueerd op een identieke manier aan de eerste deelstudie. Deelnemers voeren een \textit{think-aloud} studie uit. Dit wordt gevolgd door een half-gestructureerd interview. Vijf deelnemers moeten voldoende zijn om deze korte evaluatie uit te voeren. De resultaten van de derde deelstudie zijn beschreven in Sectie \ref{sectie:iteratiefontwerp:4validatie}.

\section{Finale evaluatie}
\label{sectie:methodologie:finale-evaluatie}

Het systeem informeert consumenten over verschillende producten. Een mogelijke finale evaluatie is een vergelijkende studie tussen het gebruik van het systeem en het gebruik van informatie aangeboden in de supermarkt. Deze aanpak wordt besproken in Sectie \ref{sectie:methodologie:finale-evaluatie:vergelijkende-studie}. Om de onderzoeksvragen te beantwoorden, gaan we na of het systeem consumenten helpt bij het begrijpen van productinformatie. Bovendien onderzoeken we of consumenten zekerder zijn van hun keuze door beter ge\"informeerd te zijn. Sectie \ref{sectie:methodologie:finale-evaluatie:protocol} bespreekt hoe dit wordt aangepakt. In Sectie \ref{chapter:eindevaluatie} worden de resultaten van de eindevaluatie besproken.

\subsection{Vergelijkende studie}
\label{sectie:methodologie:finale-evaluatie:vergelijkende-studie}

Op dit moment wordt weinig productinformatie aangeboden door supermarkten. In sommige winkels wordt gebruik gemaakt van labels zoals landen van herkomst bij wijnen. Meestal blijft productinformatie beperkt tot de informatie aangeboden door de fabrikant op verpakkingen. Een vergelijking van ons prototype met deze beperkte informatie zou te snel goede resultaten geven, omdat iedere vorm van extra informatie een verbetering is. Om deze reden gaan we ons prototype vergelijken met een andere bestaande applicatie die consumenten zouden kunnen gebruiken in een supermarkt. In Sectie \ref{sectie:literatuurstudie:bieren:informatie} werden mogelijke applicaties bekeken die gebruikt kunnen worden voor deze vergelijking.

Webshops van de corresponderende supermarkt gebruiken zou een goede mogelijkheid zijn. Deze webshops bevatten echter geen smaakinformatie of \textit{ratings}. Uit de literatuurstudie bleek deze informatie zeer belangrijk te zijn voor consumenten \cite{ChakrabortySudipta2018Dofi, GabrielyanG.2014Wtpf,KalnikaiteVaiva2013Dita}. De smartphone applicatie Untappd lijkt een betere applicatie voor de vergelijkende studie. Het bevat de meeste eigenschappen die ook in het systeem gebruikt worden. Deze eigenschappen zijn aangeduid in lichtgrijs in Tabel \ref{tab:data_eigenschappen}. Daarnaast bevat het ook het smaakprofiel en beoordelingen. Om deze reden kiezen we voor Untappd als vergelijking voor ons onderzoek.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth,height=\linewidth,keepaspectratio]{afbeeldingen/hfdst-3/app.png}
  \caption{Informatie die wordt getoond in de smartphone applicatie Untappd voor het bier Stella Artois. De applicatie toont informatie over de smaak, populariteit, alcoholpercentage, bitterheid, de brouwerij en een korte beschrijving.}
  \label{fig:untappd}
\end{figure}

Consumenten zouden de smartphone applicatie Untappd kunnen gebruiken in een supermarkt. Ze kunnen de barcode van de bieren scannen om informatie te vinden. Een voorbeeld van de informatie die wordt getoond is zichtbaar in Figuur \ref{fig:untappd}.

\subsection{Protocol finale studie}
\label{sectie:methodologie:finale-evaluatie:protocol}

Voor de aanvang van de finale studie wordt een korte introductie gegeven. Dit begint bij het tonen van een kort fragment van het keukentafelconcept van IKEA zoals beschikbaar op \textit{https://youtu.be/qD60cBQOABY} \cite{ATablefo75:online}. Hierdoor krijgen deelnemers een inzicht in de mogelijkheden van een camera-projector opstelling. Vervolgens wordt het doel van de studie uitgelegd en de manier hoe dit zal worden ge\"evalueerd. Ten slotte wordt een korte demo van het systeem en Untappd gegeven, zodat deelnemers begrijpen hoe ze met beide kunnen interageren.

\begin{figure}
  \centering
  \includegraphics[width=.7\linewidth,keepaspectratio]{afbeeldingen/hfdst-3/protocol.png}
  \caption{Het volledige protocol van de finale evaluatie bestaande uit een korte introductie, gebruik van beide systemen en vragenlijsten. De taken worden geroteerd na elke deelnemer.}
  \label{fig:protocol}
\end{figure}

De evaluatie begint met een \textit{think-aloud} studie met concrete taken. Hierdoor krijgen we inzicht in hoe goed de deelnemers de informatie begrijpen. We ontwerpen een \textit{within-subjects} studie met rotatie van twee taken om beide systemen te vergelijken, gelijkaardig aan het protocol van \citeauthor{GutierrezHernandezFrancisco2017Paph} \cite{GutierrezHernandezFrancisco2017Paph}. Dit houdt in dat de helft van de deelnemers start met de eerste taak voor het camera-projector systeem, gevolgd door de tweede taak voor Untappd. De andere helft van de deelnemers start met de eerste taak voor Untappd, gevolgd door de tweede taak voor het camera-projector systeem. De volgorde wordt geroteerd na elke deelnemer. Hierdoor test elke deelnemer alle condities.

Deelnemers moeten productkeuzes maken en uitleggen waarom ze hiervoor kozen. De taken zijn ontworpen om na te gaan hoe goed gebruikers de getoonde informatie begrijpen. Daarnaast wordt gevraagd om hun keuzes te verantwoorden en te vermelden hoe zeker ze zijn van deze keuzes. De resultaten worden gebruikt om de onderzoeksvragen te beantwoorden.

\begin{samepage}
We vragen deelnemers om realistische taken uit te voeren:
\begin{enumerate}
 \item Zoek bieren voor jezelf
  \begin{itemize}
   \item Selecteer een bier dat je zelf graag zou hebben
   \item Selecteer een gelijkaardig bier aan het bier dat je juist vond
   \item Selecteer een alternatief bier (verschillend aan de vorige twee)
   \item Vergelijk de bieren die je juist hebt gekozen. Hoe zeker ben je van je keuzes?
  \end{itemize}
 \item Zoek bieren voor vrienden die bij je thuis langskomen
  \begin{itemize}
   \item Selecteer een bier dat je vrienden graag zouden hebben
   \item Selecteer een gelijkaardig bier aan het bier dat je juist vond
   \item Selecteer een alternatief bier (verschillend aan de vorige twee)
   \item Vergelijk de bieren die je juist hebt gekozen. Hoe zeker ben je van je keuzes?
  \end{itemize}
\end{enumerate}
\end{samepage}

\newpage

De evaluatie wordt be\"eindigd door meerdere vragenlijsten. In de eerste plaats worden demografische vragen gesteld. Hier vragen we naar leeftijd, geslacht, kooppatroon van bieren, kennis van bieren en kennis van technologie. Deze vragen worden gebruikt om na te gaan dat de evaluatie geen bias heeft voor specifieke doelgroepen. Er bestaan gedetailleerde vragenlijsten om onder andere de technologiekennis van deelnemers te evalueren \cite{hosseini2012questionnaire}. We hebben ervoor gekozen deze vragenlijsten niet te gebruiken om de volledige evaluatie niet te lang te laten duren. Daarnaast zijn de korte vragen voldoende om na te gaan dat de evaluatie niet \textit{biased} is.

De tweede vragenlijst is de \textit{System Usability Scale} \cite{Brooke2006SUSA}, die wordt ingevuld voor beide systemen. Dit zijn tien vragen waarbij deelnemers kunnen kiezen tussen vijf opties van helemaal akkoord tot helemaal niet akkoord. De vragenlijst wordt gebruikt om de gebruiksvriendelijkheid van een systeem te evalueren. Hiermee verkrijgen we een score op 100, waarbij een score van 68 een gemiddelde score voorstelt \cite{BangorKortumMiller2009}. Het voordeel van de SUS-vragenlijst is dat het goede resultaten geeft voor kleinere testgroepen \cite{Brooke2006SUSA}. De SUS-score helpt bij het beantwoorden van onderzoeksvraag \ref{RQ2}. Zonder een goede gebruiksvriendelijkheid zal het systeem niet kunnen helpen bij het begrijpen van productinformatie.

Met de laatste vragenlijst wordt de ervaren werkdruk van de taken getest. Hiervoor wordt de \textit{NASA task load index} \cite{nasa-tlx} gebruikt. Deze vragenlijst bevat zeven vragen waarbij deelnemers een graad kunnen kiezen van zeer laag tot zeer hoog met 21 gradaties. De waarden worden vervolgens herleidt tot een score op 100. De aangevoelde werkdruk geeft inzicht in hoe goed deelnemers de taken konden uitvoeren. De taken geven een inzicht in hoe goed deelnemers de informatie begrijpen en hoe zeker ze zijn van hun keuzes. Hierdoor krijgen we informatie in hoe goed het verloop van de taken aanvoelde voor deelnemers. Indien het systeem gebruikers helpt bij het begrijpen van informatie en zekerheid van keuzes, dan verwachten we een lagere aangevoelde werkdruk.

Naast dit protocol worden ook objectieve metingen gedaan. Voor elke deeltaak wordt bijgehouden hoe lang deelnemers over de taak doen. Daarnaast wordt ook het aantal acties bijgehouden om de deeltaak te voltooien. De duur van de taak zal steeds aan de hogere kant liggen, omdat deelnemers tijdens de studie luidop moeten nadenken. De informatie is nog steeds nuttig om de duur van deeltaken te vergelijken.

De informatie van de \textit{think-aloud} studie zal de belangrijkste bron zijn om de onderzoeksvragen te beantwoorden. Hiermee krijgen we inzicht in hoe goed deelnemers de getoonde informatie begrijpen. In de laatste deeltaak wordt ook concreet gevraagd hoe zeker deelnemers zijn van hun keuzes. De ervaren werkdruk vragenlijst vult dit aan om een inzicht te krijgen in hoe moeilijk of makkelijk de taken zijn om uit te voeren. De SUS-vragenlijst geeft een inzicht in hoe gebruiksvriendelijk het ontwerp is. Een overzicht van het volledige protocol is zichtbaar in Figuur \ref{fig:protocol}.

De onderzoeksprocedure werd gunstig bevonden door de Sociaal-Maatschappelijke
Ethische Commissie (SMEC). Het dossier met referentienummer G-2020 01 195 is terug te vinden in Appendix \ref{app:smec}. Door de Covid-19 maatregelen waren kleine aanpassingen nodig aan de onderzoeksprocedure. De finale evaluatie werd digitaal gehouden. De grootste aanpassing is de manier waarop deelnemers worden gerekruteerd, zoals besproken in Sectie \ref{sectie:methodologie:finale-evaluatie:deelnemers}. Daarnaast werd meer uitleg gegeven aan de deelnemers wat van hen verwacht werd. Alle aanpassingen werden duidelijk vermeld aan de deelnemers van de studie.

\subsection{Deelnemers}
\label{sectie:methodologie:finale-evaluatie:deelnemers}

Het systeem zou worden opgesteld in een supermarkt om te kunnen testen met echte gebruikers. Mogelijke gebruikers zouden worden aangesproken indien ze willen deelnemen aan de studie. Als voorwaarde zou aan gebruikers worden gevraagd indien ze ooit een bier voor zichzelf of vrienden kopen. Deze voorwaarde is nodig omdat gebruikers die nooit bieren kopen niet behoren tot onze doelgroep. Alle gebruikers zouden toestemmingsformulieren invullen voor de aanvang van de studie. Deze rekruteringsmethode verzekert dat het systeem getest kan worden met willekeurige testgebruikers.

Verschillende supermarkten werden gecontacteerd als mogelijke locatie voor de finale evaluatie. Bij initi\"ele pogingen om een locatie vast te leggen was het doel en gebruik van het systeem niet duidelijk. Om deze reden kozen we ervoor om het digitale prototype beter uit te werken voor het contacteren van supermarkten.

Door de Covid-19 maatregelen werd duidelijk dat fysiek evalueren in een supermarkt niet mogelijk was. In de eerste plaats legde de Covid-19 maatregelen een hogere druk op de supermarkten. Consumenten mochten maximaal 30 minuten spenderen in de supermarkt en alleen essenti\"ele verplaatsingen waren toegelaten \cite{Coronavirus}. Ook het vastnemen van bierflesjes zou voor problemen zorgen. Daarnaast was het onduidelijk wanneer de maatregelen versoepeld zouden worden. Hierdoor werd besloten om het ontwerp aan te passen zodat het gebruikt kon worden in een webbrowser.

Op vlak van deelnemers werd de rekruteringsmethode aangepast. Deelnemers werden gerekruteerd via sociale media voor een online evaluatie. Hier zouden voornamelijk vrienden en kennissen op kunnen reageren, wat kan leiden tot bevooroordeelde resultaten. Om deze bias te minimaliseren wordt aan deelnemers gevraagd om ook familieleden of indirecte kennissen te rekruteren. Volledig willekeurige testgebruikers vinden via sociale media is niet eenvoudig. Door het maximaliseren van het aantal indirecte contacten voor de evaluatie kunnen we willekeurige testgebruikers het beste benaderen.

De finale evaluatie neemt ongeveer 20 tot 25 minuten in beslag. Alle deelnemers moeten vier taken uitvoeren volgens een \textit{think-aloud} protocol en twee vragenlijsten invullen. Dit wordt herhaald voor beide systemen te vergelijken. Daarnaast wordt ook de duur en het aantal acties bijgehouden voor elke taak. Voor het aantal deelnemers houden we rekening met de duur van de studie en de moeilijkheid om indirecte contacten te vinden. Vijftien deelnemers zou voldoende moeten zijn voor de finale evaluatie.