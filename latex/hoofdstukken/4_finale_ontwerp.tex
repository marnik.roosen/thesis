\chapter{Finaal ontwerp}
\label{chapter:finaalontwerp}
Een belangrijk onderdeel van het ontwerp van het systeem is de informatie die wordt gevisualiseerd voor de gebruiker. In dit hoofdstuk geven we een overzicht van het ontwerp van het systeem en de technische details. De implementatie van het systeem is beschikbaar op \textit{https://gitlab.com/marnik.roosen/thesis}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{afbeeldingen/hfdst-4/onderdelen_ontwerp.png}
  \caption{Het finale ontwerp van het camera-projector systeem. Bieren worden geplaatst in het centrale vak. Rond deze bieren wordt informatie geprojecteerd. Aan de rechterkant is een vergelijkend vak om bieren beter te kunnen vergelijken.}
  \label{fig:onderdelen_ontwerp}
\end{figure}

\section{Finaal ontwerp}
\label{sectie:finaalontwerp:finaalontwerp}

Het finale ontwerp evolueerde door meerdere iteraties die in detail worden besproken in Hoofdstuk \ref{chapter:iteratiefontwerp}. Het resultaat bevat een centraal vak waar bieren geplaatst kunnen worden. Hier wordt alle informatie rond de bieren gevisualiseerd. Rechts van dit centrale vak worden enkele bier attributen gevisualiseerd om makkelijker te kunnen vergelijken. Beide onderdelen worden geprojecteerd op de tafel zelf zoals zichtbaar in Figuur \ref{fig:onderdelen_ontwerp}.

Wanneer bierflesjes op de tafel worden geplaatst, zal het systeem de flesjes herkennen. Boven de bieren worden de naam en bierstijl getoond. Onder de flesjes wordt de aanwezigheid van smaak componenten getoond (afwezig tot overweldigend) in een halve radardiagram. Het alcoholpercentage van het bier wordt links gevisualiseerd in een histogram. De distributie van deze histogram is gebaseerd op een willekeurige steekproef van 5000 bieren uit een dataset van ongeveer 1.5 miljoen bieren. Gebruikers kunnen hierdoor het alcoholpercentage vergelijken ten opzichte van een algemene distributie. Rechts van het bier wordt een soortgelijke histogram getoond voor de populariteit van het bier. Ten slotte wordt de kleur van het bier getoond als de kleur van de waarde van het bier in de histogrammen en radardiagram. Figuur \ref{fig:bier_informatie} toont de informatie die wordt gevisualiseerd voor een bier in het centrale vak. De biereigenschappen in het finale ontwerp kwamen voort uit de literatuurstudie, samengevat in Tabel \ref{tab:onderzoek_eigenschappen}. Deze informatie werd ge\"itereerd na gebruikersevaluaties.

De bieren en visualisaties worden omsloten door een gekleurd kader. De kleuren komen overeen met de kleuren in het vergelijkende overzicht aan de rechterkant. Dit vergelijkend overzicht wordt zichtbaar wanneer minstens twee bieren op de tafel worden geplaatst. Om de vergelijkende kleuren extra te benadrukken wordt een legende met de dopjes van de bieren rechtsboven gevisualiseerd in dezelfde kleuren. Het vergelijkende overzicht bevat het alcoholpercentage en de populariteit als bardiagrammen. Dit zou het makkelijker maken voor gebruikers om waarden van eenzelfde eigenschap te vergelijken. Ten slotte wordt ook de smaak aanwezigheid getoond met een volledige radardiagram. De punten van elke smaak component zijn extra benadrukt om de verschillen per component extra te benadrukken. Figuur \ref{fig:vergelijkend_vak} toont de informatie die wordt gevisualiseerd in het vergelijkende vak.

Het systeem is ontworpen voor een camera-projector opstelling. Door de Covid-19 maatregelen werd fysiek testen met gebruikers niet mogelijk. Hierdoor werd het systeem aangepast zodat het ook gebruikt kan worden in een browser. Deze aanpassingen worden in detail besproken in Sectie \ref{sectie:finaalontwerp:technisch:webbrowser}.

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth,height=\linewidth,keepaspectratio]{afbeeldingen/hfdst-4/stella.PNG}
  \caption{Informatie die wordt gevisualiseerd rond bieren in het centrale vak. Gebruikers zien informatie over de smaak, het alcoholpercentage, de populariteit, bierstijl en de bierkleur.}
  \label{fig:bier_informatie}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth,height=\linewidth,keepaspectratio]{afbeeldingen/hfdst-4/vergelijking.PNG}
  \caption{Informatie die wordt getoond in het vergelijkende vak. Data van het alcoholpercentage, de populariteit en de smaak worden dicht bij elkaar gevisualiseerd, waardoor gebruikers beter kunnen vergelijken.}
  \label{fig:vergelijkend_vak}
\end{figure}

\section{Technische details}
\label{sectie:finaalontwerp:technisch}

In deze sectie bespreken we de technische details van het ontwerp. We bespreken eerst de uitvoering van het camera-projector ontwerp. Vervolgens worden de aanpassingen besproken voor het \textit{webbrowser} ontwerp. 

\subsection{Camera-projector implementatie}
\label{sectie:finaalontwerp:technisch:cameraprojector}

De camera-projector implementatie is opgebouwd uit verschillende onderdelen. In de eerste plaats moeten het systeem worden opgesteld en het beeld gekalibreerd. Vervolgens moeten bierflesjes door de camera worden herkend. Ten slotte worden visualisaties geprojecteerd.

\subsubsection{Opstelling en kalibratie}
\label{sectie:finaalontwerp:technisch:cameraprojector:kalibratie}

Het systeem gebruikt een standaard webcam, namelijk een Logitech c930e camera. Deze heeft een resolutie van 1920 $\times$ 1080 en een kijk veld van 90 graden. Voor de projecties wordt een Acer H6517ST projector gebruikt. Dit is een korte-afstand projector met een resolutie van 1920 $\times$ 1080 en 3000 lumen. Ten slotte wordt een laptop gebruikt waarop beide apparaten worden aangesloten en waarop de applicatie draait. Dit is een standaard opstelling voor een camera-projector systeem \cite{YamamotoGoshiro2015Auid}. De camera en projector worden aan het plafond gemonteerd, boven een tafel.

De camera en het geprojecteerde beeld overlappen niet een-op-een met elkaar. Het camerabeeld is namelijk iets groter dan het geprojecteerde beeld. Een kalibratie-stap is nodig om het exacte beeld van de projector uit het camerabeeld te segmenteren. Dit is noodzakelijk om de exacte locatie van de flesjes te bepalen. Zonder deze exacte locatie kunnen de visualisaties niet op de correcte plaats geprojecteerd worden. Hiervoor maken we gebruik van de \textit{OpenCV.js (3.4.10) \cite{OpenCV} JavaScript library}.

Kalibratie is mogelijk door initieel een volledig wit beeld te projecteren. Verschillende functies in de \textit{OpenCV.js library} kunnen dit witte, geprojecteerde beeld herkennen en segmenteren. In onze implementatie maken we gebruik van een \textit{threshold} \cite{threshold} functie om alleen pixels met een hoge intensiteit te tonen. Vervolgens kan het witte beeld gesegmenteerd worden met de \textit{findContours} \cite{FindContours} functie. Alle toekomstige beelden moeten steeds dezelfde vorm uitsnijden om het geprojecteerde beeld te verkrijgen. 

De projector kan ingesteld worden zodat het geprojecteerde beeld een rechthoekige vorm krijgt. Een perspectieftransformatie wordt toegepast op het gesegmenteerde beeld om te verzekeren dat dit beeld perfect rechthoekig is. Hierdoor krijgen we het geprojecteerde beeld, zoals gezien door de camera, dat een-op-een overeen komt met het uitgevoerde beeld van de applicatie. Het stappenplan van de kalibratie wordt weergegeven in Figuur \ref{fig:kalibratie}.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{afbeeldingen/hfdst-4/kalibratie.png}
  \caption{Het stappenplan om het geprojecteerde beeld van het camerabeeld te segmenteren.}
  \label{fig:kalibratie}
\end{figure}

\subsubsection{Herkenning van bierflesjes}
\label{sectie:finaalontwerp:technisch:cameraprojector:herkenning}

De herkenning van bieren gebeurt met \textit{OpenCV.js} via stapsgewijze verwerking van de beelden. Afbeeldingen moeten pas verwerkt worden wanneer een verandering gebeurt in het beeld. Veranderingen worden waargenomen door periodiek afbeeldingen te vergelijken via een afstandsfunctie van \textit{OpenCV.js}. Zo een afstandsfunctie berekent het verschil tussen twee afbeeldingen als een afstand. Als de waarde groot genoeg is, dan is het beeld veranderd en moet het nieuwste beeld verwerkt worden.

Bieren die in het projectieveld geplaatst worden, zullen worden waargenomen door de camera. De bieren kunnen worden herkend via de dopjes op de flesjes. Er wordt gebruik gemaakt van \textit{HoughCircles} \cite{HoughCircles} en \textit{findContours} \cite{FindContours} functies van \textit{OpenCV.js} om de dopjes uit het beeld te segmenteren. Dit zijn functies om cirkels en vormen te herkennen in afbeeldingen. Beide methodes zijn noodzakelijk om beter bestand te zijn tegen ruis in de afbeelding. Eerst kunnen grotere vormen worden herkend in de afbeelding. Vervolgens kan de exacte vorm van de dopjes gevonden worden door te zoeken naar cirkels.

De classificatie van dopjes van op verschillende manieren gebeuren. De eerste uitvoering maakt gebruik van een simpele afbeelding subtractie. Deze uitvoering geeft enkel goede resultaten voor een beperkt aantal dopjes. Verbeteringen zijn mogelijk door het gebruik van \textit{k-Nearest Neigbours} classificatie zoals beschikbaar als functie in \textit{OpenCV.js} \cite{Classify_2020}. Een andere mogelijkheid is het gebruiken van een bestaande \textit{Deep Learning API} voor bier classificatie zoals \cite{Beerminder}.

\subsubsection{Visualisatie van informatie}
\label{sectie:finaalontwerp:technisch:cameraprojector:visualisatie}

De visualisaties zijn ge\"implementeerd met de \textit{D3.js (5.16.0) \cite{D3} JavaScript library}. Bierdata is opgeslagen in csv bestanden en wordt ingelezen met D3. Tijdens de initialisatie wordt de data opgeslagen in \textit{arrays} per stijl, smaak, alcohol, enzovoort. Deze initialisatie stap duurt iets langer, maar data van herkende bieren kan sneller gelezen worden tijdens het gebruik.

De histogrammen en bardiagrammen zijn handmatig ge\"implementeerd via de \textit{D3.js}. Het radardiagram voor de smaak is gebaseerd op \cite{radarchart}. Deze uitvoering werd aangepast om halve radardiagrammen te visualiseren. Alle andere visualisaties gebeuren via rechtstreekse functies van D3.

\subsection{\textit{Webbrowser} implementatie door Covid-19 maatregelen}
\label{sectie:finaalontwerp:technisch:webbrowser}

Door de Covid-19 maatregelen werd duidelijk dat fysiek evalueren in een supermarkt niet mogelijk was. Consumenten mochten maximaal 30 minuten spenderen in de supermarkt en alleen essenti\"ele verplaatsingen waren toegelaten \cite{Coronavirus}. Ook het vastnemen van bierflesjes zou voor problemen zorgen. We bespreken daarom de aanpassingen van het ontwerp om gebruikt te kunnen worden in een \textit{webbrowser}. De grootste aanpassing is de toevoeging van sleepacties om afbeeldingen van bierflesjes te verplaatsen, gelijkaardig aan het verplaatsen van bierflesjes in een supermarkt. Daarnaast werden de \textit{labels} bierrek en tafel toegevoegd om het onderscheid te verduidelijken voor de gebruikers. Naast deze aanpassingen is het \textit{webbrowser} ontwerp identiek aan het camera-projector ontwerp.

\subsubsection{Verplaatsen van bierflesjes}
\label{sectie:finaalontwerp:technisch:slepen}

In de \textit{webbrowser} uitvoering kunnen gebruikers afbeeldingen van bierflesjes verslepen naar het centrale vak om informatie te zien zoals zichtbaar in Figuur \ref{fig:web-browser}. Het verslepen van afbeeldingen maakt gebruik van de \textit{Interact.js (1.9.13) \cite{interactjs_2012} JavaScript library}. Voor gebruiksvriendelijkheid bevinden de verschillende flesjes zich in een \textit{scrollable} container. Dit stelt het bierrek voor waaruit gebruikers bieren kunnen nemen. Gebruikers kunnen \textit{scrollen} in deze container om hun gewenste flesje te zoeken, om vervolgens te slepen naar het statische centrale vak. 

In normale omstandigheden is het niet mogelijk om een object uit een \textit{scrollable} container te slepen. De container vergroot het gebied waarbinnen de gebruiker van \textit{scrollen}, maar het object verlaat de container niet. Het systeem maakt gebruik van de kloning technologie van \textit{Interact.js} om toch buiten de container te kunnen komen. Bij de start van de verplaatsing wordt de originele afbeelding van het flesje verborgen. Een kloon van de afbeelding wordt toegevoegd aan de pagina op dezelfde positie. De gebruiker sleept de gekloonde afbeelding verder alsof het de originele afbeelding is. De kloon wordt verwijderd wanneer deze uit het centrale vak wordt versleept. De originele afbeelding wordt terug getoond op zijn standaardpositie.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{afbeeldingen/hfdst-4/interface_01_05_20.PNG}
  \caption{Het \textit{webbrowser} ontwerp met een \textit{scrollable} container als bierrek en een statische tafel. Het camera-projector ontwerp werd hier extra omkaderd om duidelijk te tonen welke aanvullingen werden gedaan bij het \textit{webbrowser} ontwerp.}
  \label{fig:web-browser}
\end{figure}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
