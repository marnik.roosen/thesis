var Histogram = {
  draw: function (id, parent_svg, data, focus, options) {
    var cfg = {
      name: "histogram",
      translate_x: 0,
      translate_y: 0,
      name_translate_x: 0,
      name_translate_y: -5,
      max_value: 60,
      width: 200,
      height: 100,
      color: "grey",
      focus_color: "red",
      unit: "",
      type: "",
      opacity: 0.7,
      svg_scale: 1,
      font_size: "20",
      ticks: 10,
      y_ticks: 0,
      x_ticks: 0
    };
    if ("undefined" !== typeof options) {
      for (var i in options) {
        if ("undefined" !== typeof options[i]) {
          cfg[i] = options[i];
        }
      }
    }

    // initialize svg in the given id
    var svg = parent_svg
      .append("g")
      .attr("id", cfg.name)
      .attr("class", "barchart" + " " + id)
      .attr(
        "transform",
        "translate(" + cfg.translate_x + "," + cfg.translate_y + ")"
      );

    // set the ranges
    var x = d3.scaleLinear()
      .domain([0, cfg.max_value])
      .range([0, cfg.width]);
    var y = d3.scaleLinear()
      .range([cfg.height, 0]);

    // set the parameters for the histogram
    var histogram = d3.histogram()
      .value(function (d) { return d; })
      .domain(x.domain())
      .thresholds(x.ticks(cfg.ticks));


    // group the data for the bars
    var bins = histogram(data);

    // Scale the range of the data in the y domain
    y.domain([0, d3.max(bins, function (d) { return d.length; })]);

    // append the bar rectangles to the svg element
    svg.selectAll("rect")
      .data(bins)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("x", 1)
      .attr("transform", function (d) {
        return "translate(" + x(d.x0) + "," + y(d.length) + ")";
      })
      .attr("width", function (d) {
        var width = x(d.x1) - x(d.x0) - 1;
        if (width > 0) {
          return x(d.x1) - x(d.x0) - 1;
        } else {
          return 0;
        }
      })
      .attr("height", function (d) { return cfg.height - y(d.length); })
      .style("fill", function (d) {
        if (focus >= d.x0 && focus < d.x1) {
          return cfg.focus_color;
        } else {
          return cfg.color;
        }
      })
      .style("fill-opacity", cfg.opacity)
      .attr("rx", 1)
      .attr("ry", 1);;

    // add the x Axis
    svg.append("g")
      .attr("transform", "translate(0," + cfg.height + ")")
      .call(d3.axisBottom(x).tickValues([0, focus, cfg.max_value]).tickFormat(d => d + cfg.unit))
      .style("opacity", cfg.opacity)
      .style("font-size", cfg.font_size * 0.75);


    // add the y Axis
    /*svg.append("g")
      .call(d3.axisLeft(y).ticks(cfg.y_ticks))
      .style("opacity", 0.6);*/

    // add label
    svg
      .append("text")
      .text(cfg.type)
      .attr("id", "text-id")
      .style("text-anchor", "middle")
      .style("font-size", cfg.font_size)
      .attr("x", cfg.name_translate_x + cfg.width / 2)
      .attr("y", cfg.name_translate_y);
  }
}