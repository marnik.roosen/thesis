var ArcChart = {
  draw: function (id, data, options) {
    var cfg = {
      max: d3.max(data),
      svg_width: 600,
      svg_height: 600,
      width: 500,
      height: 40,
      x_offset: 0,
      unit: "%",
      type: "ABV",
      image: ""
    };
    if ("undefined" !== typeof options) {
      for (var i in options) {
        if ("undefined" !== typeof options[i]) {
          cfg[i] = options[i];
        }
      }
    }

    var width = 960,
      height = 450,
      radius = Math.min(width, height) / 2;

    var svg = d3
      .select(id)
      .append("svg")
      .attr("width", cfg.svg_width)
      .attr("height", cfg.svg_height)
      .attr(
        "transform",
        "translate(100,100)"
      );

    var pie = d3.layout.pie()
      .sort(null)
      .value(function (d) {
        return d.value;
      });

    var arc = d3.svg.arc()
      .outerRadius(radius * 0.8)
      .innerRadius(radius * 0.65);

    var outerArc = d3.svg.arc()
      .innerRadius(radius * 0.9)
      .outerRadius(radius * 0.9);

    var key = function (d) { return d.data.label; };

    var color = d3.scale.ordinal()
      .domain(["Lorem ipsum", "dolor sit", "amet", "consectetur", "adipisicing", "elit", "sed", "do", "eiusmod", "tempor", "incididunt"])
      .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

    function randomData() {
      var labels = color.domain();
      return labels.map(function (label) {
        return { label: label, value: Math.random() }
      });
    }

    svg.append("g")
      .attr("class", "slices");
    svg.append("g")
      .attr("class", "labels");
    svg.append("g")
      .attr("class", "lines");

    change(randomData());




    function change(data) {

      /* ------- PIE SLICES -------*/
      var slice = svg.select(".slices").selectAll("path.slice")
        .data(pie(data), key);

      slice.enter()
        .insert("path")
        .style("fill", function (d) { return color(d.data.label); })
        .attr("class", "slice");

      slice
        .transition().duration(1000)
        .attrTween("d", function (d) {
          this._current = this._current || d;
          var interpolate = d3.interpolate(this._current, d);
          this._current = interpolate(0);
          return function (t) {
            return arc(interpolate(t));
          };
        })

      slice.exit()
        .remove();
    }

  }
};