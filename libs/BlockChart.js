var BlockChart = {
  draw: function (id, data, options) {
    var cfg = {
      max: d3.max(data),
      svg_width: 300,
      svg_height: 150,
      width: 300,
      height: 150,
      x_offset: 0,
      y_offset: 50,
      x_scale: 60,
      y_scale: 35,
      block_width: 30,
      block_height: 30,
      unit: "%",
      type: "ABV",
      image: ""
    };
    if ("undefined" !== typeof options) {
      for (var i in options) {
        if ("undefined" !== typeof options[i]) {
          cfg[i] = options[i];
        }
      }
    }

    var coord_data = [];
    var labels = [];
    data.sort(function (a, b) {
      return d3.ascending(a.value, b.value);
    }).forEach(function (d, i) {
      if (coord_data.length === 0) {
        coord_data.push({ id: d.id, value: d.value, x: 0, y: 0 });
        labels.push({ id: d.id, value: d.value, x: 0, y: 0 })
      } else {
        var last = coord_data[coord_data.length - 1];
        if (last.value === d.value) {
          coord_data.push({ id: d.id, value: d.value, x: last.x, y: last.y + 1 });
        } else {
          coord_data.push({ id: d.id, value: d.value, x: last.x + 1, y: 0 });
          labels.push({ id: d.id, value: d.value, x: last.x + 1, y: 0 })
        }
      }
    });

    cfg.svg_height = d3.max(coord_data, function (d) { return +d.y; }) * cfg.y_scale * 1.5 + cfg.y_offset + cfg.block_height
    cfg.svg_width = d3.max(coord_data, function (d) { return +d.x; }) * cfg.x_scale * 1.5 + cfg.x_offset + cfg.block_width

    var svg = d3
      .select(id)
      .append("svg")
      .attr("width", cfg.svg_width)
      .attr("height", cfg.svg_height);

    // data points
    svg
      .selectAll("rect")
      .data(coord_data)
      .enter()
      .append("rect")
      .attr("x", (d) => cfg.x_offset + cfg.x_scale * d.x)
      .attr("y", (d) => cfg.y_offset + cfg.y_scale * d.y)
      .attr("height", cfg.block_height)
      .attr("width", cfg.block_width)
      .style("fill", function (d) {
        return colorscale(d.id);
      });

    // data values
    svg
      .selectAll("text")
      .data(labels)
      .enter()
      .append("text")
      .text(d => d.value + " " + cfg.unit)
      .attr("x", (d, i) => cfg.x_offset + d.x * cfg.x_scale + 15)
      .attr("y", (d, i) => cfg.y_offset + d.y * cfg.y_scale - 10)
      .style("text-anchor", "middle")
      .call(wrap, 20);
  }

};

function wrap(text, width) {
  text.each(function () {
    let text = d3.select(this),
      words = text.text().split(/\s+/),
      word,
      line = [],
      lineNumber = 0,
      lineHeight = 0.9, // ems
      x = text.attr("x"),
      y = text.attr("y"),
      dy = 1.0,
      tspan = text.text(null).append("tspan").attr("x", x).attr("y", y);
    words.pop();
    console.log(words);
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", -(++lineNumber * lineHeight - dy) + "em").text(word);
      }
    }
  });
}