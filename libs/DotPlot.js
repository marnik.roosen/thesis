var DotPlot = {
  draw: function (id, parent_id, data, focus, options) {
    var cfg = {
      name: "barchart",
      max_value: d3.max(data),
      fill: false,
      width: 150,
      datapoint_size: 2,
      translate_x: 0,
      translate_y: 0,
      label_translate_x: 0,
      label_translate_y: 15,
      dots_translate_x: 0,
      dots_translate_y: 5,
      name_translate_x: 0,
      name_translate_y: 12,
      x_ticks: 0,
      rotation: 0,
      text_rotation: 0,
      unit: "%",
      type: "ABV",
      image: "",
      color: "black",
      focus_color: "red",
      label_color: "black",
      opacity: 0.7,
      svg_scale: 1,
      text_scale: 1,
      unit_labels: false
    };
    if ("undefined" !== typeof options) {
      for (var i in options) {
        if ("undefined" !== typeof options[i]) {
          cfg[i] = options[i];
        }
      }
    }

    // focus dot (colored as focus_color) always on top
    data.push(focus);

    // all labels are rotated with regards to the total svg rotation
    cfg.text_rotation = 360 - cfg.rotation;

    // initialize svg in the given id
    var svg = d3
      .select(parent_id)
      .append("g")
      .attr("id", cfg.name)
      .attr("class", "dotplot" + " " + id)
      .attr(
        "transform",
        "translate(" + cfg.translate_x + "," + (cfg.translate_y) + ") rotate(" + cfg.rotation + ")"
      );

    // linear scale for the bar chart
    var plot_scale = d3.scaleLinear()
      .domain([0, cfg.max_value])
      .range([0, cfg.width]);

    // add the x Axis
    svg.append("g")
      .call(
        d3
          .axisBottom(plot_scale)
          .tickValues([0, focus, cfg.max_value])
          .tickFormat(d => {
            if (d == cfg.max_value || d == focus) {
              return d + cfg.unit;
            } else {
              return d;
            }
          })
      )
      .style("opacity", cfg.opacity)
      .style("font-size", 5 + 5 * cfg.text_scale + "px")
      .selectAll("text");

    // add data as nodes
    svg.
      selectAll(".nodes")
      .data(data)
      .enter()
      .append("svg:circle")
      .attr("r", function (d, i) {
        if (d === focus) {
          return cfg.datapoint_size + 2;
        } else {
          return cfg.datapoint_size;
        }
      })
      .attr("cx", d => plot_scale(d) + cfg.dots_translate_x)
      .attr("cy", d => -cfg.dots_translate_y * cfg.svg_scale)
      .style("fill", function (d, i) {
        if (d === focus) {
          return cfg.focus_color;
        } else {
          return cfg.color;
        }
      })
      .style("fill-opacity", cfg.opacity);


    // add data values as text => hidden to not show values of all data points
    /*svg
      .append("text")
      .text(focus + cfg.unit)
      .attr(
        "transform",  // rotate around it's own location
        "translate(" +
        (plot_scale(focus) + cfg.label_translate_x) +
        ", " +
        (cfg.label_translate_y) +
        ") rotate(" + cfg.text_rotation + ")"
      )
      .attr("class", "text")
      .style("text-anchor", "middle")
      .style("font-family", "sans-serif")
      .style("font-size", 13 * cfg.text_scale + "px")
      .style("fill", cfg.label_color)
      .style("fill-opacity", 1);*/

    // add barchart name
    svg
      .append("text")
      .attr(
        "transform",  // rotate around it's own location
        d => "translate(" +
          (cfg.name_translate_x + cfg.width / 2) +
          ", " +
          (-cfg.name_translate_y * cfg.svg_scale) +
          ") rotate(" + cfg.text_rotation + ")"
      )
      .text(cfg.type)
      .attr("class", "text")
      .style("text-anchor", "middle")
      .style("font-family", "sans-serif")
      .style("font-size", 10 + 5 * cfg.text_scale + "px");
  }
}