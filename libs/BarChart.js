var BarChart = {
  draw: function (id, parent_svg, data, options) {
    var cfg = {
      name: "barchart",
      max_value: d3.max(data),
      fill: false,
      bar_width: 200,
      bar_height: 15,
      data_width: 3,
      translate_x: 10,
      translate_y: 0,
      label_translate_x: 5,
      label_translate_y: 0,
      name_translate_x: 0,
      name_translate_y: -10,
      rotation: 0,
      text_rotation: 0,
      bar_color: "",
      unit: "%",
      type: "ABV",
      image: "",
      color: "black",
      color_scale: "",
      label_color: "black",
      opacity: 0.7,
      svg_scale: 1,
      font_size: "20",
      unit_labels: false,
      margin: 10
    };
    if ("undefined" !== typeof options) {
      for (var i in options) {
        if ("undefined" !== typeof options[i]) {
          cfg[i] = options[i];
        }
      }
    }

    // all labels are rotated with regards to the total svg rotation
    cfg.text_rotation = 360 - cfg.rotation;

    // initialize svg in the given id
    var svg = parent_svg
      .append("g")
      .attr("id", cfg.name)
      .attr("class", "barchart" + " " + id)
      .attr(
        "transform",
        "translate(" + cfg.translate_x + "," + (20 + cfg.translate_y) + ") rotate(" + cfg.rotation + ")"
      );

    // linear scale for the bar chart
    var bar_scale = d3.scaleLinear()
      .domain([0, cfg.max_value])
      .range([0, cfg.bar_width * cfg.svg_scale]);

    // initialize the big bar using an image or as a simple rectangle
    for (let i = 0; i < data.length; i++) {

      // add data points
      var color;
      if (cfg.color_scale !== "") {
        color = cfg.color_scale(i);
      } else {
        color = cfg.color;
      }
      svg
        .append("rect")
        .attr("height", cfg.bar_height * cfg.svg_scale)
        .attr("width", bar_scale(data[i].value))
        .attr('fill', color)
        .style("fill-opacity", cfg.opacity)
        .attr('stroke', cfg.bar_color)
        .attr(
          "transform",
          "translate(" + 0 + "," + (((cfg.margin + 10) * i) + (cfg.margin / 2) - (cfg.bar_height / 2)) + ")"
        )
        .attr("rx", 1)
        .attr("ry", 1);


      // ability to add an image before the charts
      /*svg
        .append("svg:image")
        .attr("xlink:href", data[i].image)
        .attr("class", "img")
        .attr("width", cfg.margin + "px")
        .attr("height", cfg.margin + "px")
        .attr(
          "transform",
          "translate(" + 0 + "," + ((cfg.margin + 10) * i) + ")"
        );*/


      // add data values as text
      svg
        .append("text")
        .text(+data[i].value + cfg.unit)
        .attr(
          "transform",  // rotate around it's own location
          d => "translate(" +
            (bar_scale(data[i].value) + cfg.label_translate_x) +
            ", " + (((cfg.margin + 10) * i) + (cfg.margin / 2) + (cfg.bar_height / 3)) + ") rotate(" + cfg.text_rotation + ")"
        )
        .attr("class", "text")
        .style("font-family", "sans-serif")
        .style("font-size", cfg.font_size * 0.75)
        .style("fill", cfg.label_color)
        .style("fill-opacity", 1);

    }


    // add barchart name
    svg
      .append("text")
      .attr(
        "transform",  // rotate around it's own location
        d => "translate(" +
          (cfg.name_translate_x) +
          ", " +
          (cfg.name_translate_y) +
          ") rotate(" + cfg.text_rotation + ")"
      )
      .text(cfg.type)
      .attr("class", "text")
      .style("text-anchor", "middle")
      .style("font-family", "sans-serif")
      .style("font-size", cfg.font_size);
  }
}