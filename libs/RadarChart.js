//Practically all this code comes from https://github.com/alangrafu/radar-chart-d3
//I only made some additions and aesthetic adjustments to make the chart look better
//(of course, that is only my point of view)
//Such as a better placement of the titles at each line end,
//adding numbers that reflect what each circular level stands for
//Not placing the last level and slight differences in color
//
//For a bit of extra information check the blog about it:
//http://nbremer.blogspot.nl/2013/09/making-d3-radar-chart-look-bit-better.html

var RadarChart = {
  draw: function (id, parent_svg, data, options) {
    var cfg = {
      datapoint_size: 3,
      radar_radius: 150,  // radar width = radar height = radar radius * 2
      text_scale: 1.0,
      data_levels: 3,
      max_value: 0,
      radians: 0.5 * Math.PI,
      svg_rotation: 250,
      opacityArea: 0,
      label_translate_x: 0,
      label_translate_y: 0,
      category_translate_x: 0,
      category_translate_y: -20,
      stroke_width: "1.5px",
      category_scale: 1.0,
      text_rotation: 0,
      explanation_rotation: 0,
      translate_x: 50,
      translate_y: 50,
      color: "black",
      color_scale: "",
      explanation_label: "",
      fill: false,
      font_size: "20"
    };

    if ("undefined" !== typeof options) {
      for (var i in options) {
        if ("undefined" !== typeof options[i]) {
          cfg[i] = options[i];
        }
      }
    }
    cfg.max_value = Math.max(
      cfg.max_value,
      d3.max(data, function (i) {
        return d3.max(
          i.map(function (o) {
            return o.value;
          })
        );
      })
    );

    // all labels are rotated with regards to the total svg rotation
    cfg.text_rotation = 360 - cfg.svg_rotation;

    // all labels are rotated with regards to the total svg rotation
    cfg.explanation_rotation = 90 + 180 - (180 * cfg.radians / Math.PI);

    // take the first datasample to retrieve the categories
    var categories = data[0].map(d => d.axis);

    // If the chart isn't a whole circle, we shouldn't connect the last to the first category
    if (cfg.radians !== 2 * Math.PI) {
      var connections_count = categories.length - 1;
    } else {
      var connections_count = categories.length;
    }

    // append a svg to the given id, rotation and translation w.r.t. the 0-point of the radarchart (middle of full chart)
    var svg = parent_svg
      .append("g")
      .attr("class", "radarchart" + " " + id)
      .attr(
        "transform",
        "translate(" + (cfg.translate_x - cfg.radar_radius) + "," + (cfg.translate_y - cfg.radar_radius) + ") rotate(" + cfg.svg_rotation + "," + cfg.radar_radius + "," + cfg.radar_radius + ")"
      );

    // Thin lines indicating the levels as ploygons
    //    If the chart isn't a whole circle, we should leave out the last set of lines (which would close the circle)
    if (cfg.radians !== 2 * Math.PI) var thin_lines_data = categories.slice(0, -1);
    else var thin_lines_data = categories;

    for (var level = 0; level < cfg.data_levels; level++) {
      // position of each level relative to the size of the radarchart
      var levelFactor = cfg.radar_radius * ((level + 1) / cfg.data_levels);
      svg.selectAll(".levels")
        .data(thin_lines_data)
        .enter()
        .append("svg:line")
        .attr("x1", (d, i) => levelFactor * (1 - Math.sin(i * cfg.radians / connections_count)))
        .attr("y1", (d, i) => levelFactor * (1 - Math.cos(i * cfg.radians / connections_count)))
        .attr("x2", (d, i) => levelFactor * (1 - Math.sin((i + 1) * cfg.radians / connections_count)))
        .attr("y2", (d, i) => levelFactor * (1 - Math.cos((i + 1) * cfg.radians / connections_count)))
        .attr("class", "line")
        .style("stroke", "grey")
        .style("stroke-opacity", "0.5")
        .style("stroke-width", "0.4px")
        .attr(
          "transform",
          "translate(" +
          (cfg.radar_radius - levelFactor) +
          ", " +
          (cfg.radar_radius - levelFactor) +
          ")"
        );
    }

    // Text indicating the level values
    /*for (var level = 0; level < cfg.data_levels; level++) {
      var levelFactor = cfg.radar_radius * ((level + 1) / cfg.data_levels);
      svg.selectAll(".levels")
        .data([1]) //dummy data
        .enter()
        .append("svg:text")
        .attr(
          "transform",  // rotate around it's own location
          "translate(" +
          (cfg.radar_radius + cfg.label_translate_x) +
          ", " +
          (cfg.radar_radius - levelFactor + cfg.label_translate_y) +
          ") rotate(" + cfg.text_rotation + ")"
        )
        .attr("class", "text")
        .style("font-family", "sans-serif")
        .style("font-size", cfg.font_size)
        .text(d3.format("")((level + 1) * cfg.max_value / cfg.data_levels));

    }*/

    if (cfg.explanation_label !== "") {
      // Explanation label
      svg.append("svg:text")
        .attr(
          "transform",  // rotate around it's own location
          "translate(" +
          (cfg.radar_radius + 10) +
          ", " +
          (cfg.radar_radius + 0) +
          ") rotate(" + cfg.explanation_rotation + ")"
        )
        .attr("class", "text")
        .style("text-anchor", "middle")
        .style("font-family", "sans-serif")
        .style("font-size", cfg.font_size)
        .text(cfg.explanation_label);
    }




    // constructing the main axis of each category
    var axis = svg
      .selectAll(".axis")
      .data(categories)
      .enter()
      .append("g")
      .attr("class", "axis");
    axis
      .append("line")
      .attr("x1", cfg.radar_radius)
      .attr("y1", cfg.radar_radius)
      .attr("x2", (d, i) => cfg.radar_radius * (1 - Math.sin((i * cfg.radians) / connections_count)))
      .attr("y2", (d, i) => cfg.radar_radius * (1 - Math.cos((i * cfg.radians) / connections_count)))
      .attr("class", "line")
      .style("stroke", "grey")
      .style("stroke-width", "1px")
      .style("stroke-opacity", "0.5");

    // attaching the category name at the end of each axis
    axis
      .append("text")
      .attr("class", "legend")
      .text(d => d)
      .style("font-family", "sans-serif")
      .style("font-size", cfg.font_size)
      .attr("text-anchor", "middle")
      .attr("class", "text")
      .attr("dy", "1.5em")
      .attr("transform", (d, i) =>
        "translate(" + (cfg.radar_radius * (1 - cfg.category_scale * Math.sin((i * cfg.radians) / connections_count))) + ","
        + (cfg.radar_radius * (1 - cfg.category_scale * Math.cos((i * cfg.radians) / connections_count)))
        + ") rotate(" + cfg.text_rotation + ") translate(" + cfg.category_translate_x + "," + cfg.category_translate_y + ")");



    // adding all datapoints as lines:
    //    generating an array with coordinates for each datapoint
    series = 0;
    data.forEach(function (y, i) {
      var color;
      if (cfg.color_scale !== "") {
        color = cfg.color_scale(i);
      } else {
        color = cfg.color;
      }

      dataValues = [];
      svg.selectAll(".nodes").data(y, function (j, i) {
        dataValues.push([
          (cfg.radar_radius) *
          (1 -
            (parseFloat(Math.max(j.value, 0)) / cfg.max_value) *
            Math.sin((i * cfg.radians) / connections_count)),
          (cfg.radar_radius) *
          (1 -
            (parseFloat(Math.max(j.value, 0)) / cfg.max_value) *
            Math.cos((i * cfg.radians) / connections_count))
        ]);
      });

      //    if the chart is a circle, the last and first datapoint needs to be connected as well
      if (cfg.radians == 2 * Math.PI) (dataValues.push(dataValues[0]));

      //    pairwise addition of lines
      pairs = d3.pairs(dataValues);
      svg.selectAll(".lines")
        .data(pairs)
        .enter()
        .append("svg:line")
        .attr("x1", function (d) { return d[0][0]; })
        .attr("x2", function (d) { return d[1][0]; })
        .attr("y1", function (d) { return d[0][1]; })
        .attr("y2", function (d) { return d[1][1]; })
        .attr("class", "line")
        .style("stroke", color)
        .style("stroke-opacity", "1")
        .style("stroke-width", cfg.stroke_width);

      if (cfg.fill) {
        dataValues.push([cfg.radar_radius, cfg.radar_radius]);
        svg.selectAll("polygon")
          .data([dataValues])
          .enter()
          .append("polygon")
          .attr("points", function (d) {
            return d.map(function (d) {
              return [d[0], d[1]].join(",");
            }).join(" ");
          })
          .style("fill", color)
          .style("fill-opacity", 0.8);
      }

      series++;
    });
    series = 0;

    // adding all datapoints as dots
    data.forEach(function (y, i) {
      var color;
      if (cfg.color_scale !== "") {
        color = cfg.color_scale(i);
      } else {
        color = cfg.color;
      }
      svg.selectAll(".nodes")
        .data(y)
        .enter()
        .append("svg:circle")
        .attr("class", "radar-chart-serie" + series)
        .attr("r", cfg.datapoint_size)
        .attr("alt", function (j) {
          return Math.max(j.value, 0);
        })
        .attr("cx", function (j, i) {
          dataValues.push([
            (cfg.radar_radius) *
            (1 -
              (parseFloat(Math.max(j.value, 0)) / cfg.max_value) *
              Math.sin((i * cfg.radians) / connections_count)),
            (cfg.radar_radius) *
            (1 -
              (parseFloat(Math.max(j.value, 0)) / cfg.max_value) *
              Math.cos((i * cfg.radians) / connections_count))
          ]);
          return (
            (cfg.radar_radius) *
            (1 -
              (Math.max(j.value, 0) / cfg.max_value) *
              Math.sin((i * cfg.radians) / connections_count))
          );
        })
        .attr("cy", function (j, i) {
          return (
            (cfg.radar_radius) *
            (1 -
              (Math.max(j.value, 0) / cfg.max_value) *
              Math.cos((i * cfg.radians) / connections_count))
          );
        })
        .attr("data-id", function (j) {
          return j.axis;
        })
        .style("fill", color)
        .style("fill-opacity", 0.9)
        .append("svg:title")
        .attr("class", "text")
        .text(function (j) {
          return Math.max(j.value, 0);
        });

      series++;
    });


  }
};
