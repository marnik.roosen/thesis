var Legend = {
  draw: function (id, data, options) {
    var cfg = {
      translate_x: 50,
      translate_y: 50,
      name_translate_x: 0,
      name_translate_y: 0,
      svg_rotation: 0,
      text_rotation: 0,
      svg_scale: 1,
      text_scale: 1,
      color: d3.scaleOrdinal(d3.schemeCategory10),
      type: "Legende"
    };

    if ("undefined" !== typeof options) {
      for (var i in options) {
        if ("undefined" !== typeof options[i]) {
          cfg[i] = options[i];
        }
      }
    }

    // append a svg to the given id, rotation and translation
    var svg = d3
      .select(id)
      .append("g")
      .attr("class", "legend")
      .attr(
        "transform",
        "translate(" + (cfg.translate_x) + "," + (cfg.translate_y * cfg.svg_scale) + ") rotate(" + cfg.svg_rotation + ")"
      );

    // Create the title for the legend
    // add barchart name
    svg
      .append("text")
      .attr(
        "transform",  // rotate around it's own location
        d => "translate(" +
          (cfg.name_translate_x) +
          ", " +
          (cfg.name_translate_y * cfg.svg_scale) +
          ") rotate(" + cfg.text_rotation + ")"
      )
      .text(cfg.type)
      .attr("class", "text")
      .style("text-anchor", "left")
      .style("font-family", "sans-serif")
      .style("font-size", 12 + 5 * cfg.text_scale + "px");

    // Initiate Legend datapoints
    var legend = svg
      .append("g")
      .attr("class", "legend");

    // Create colour squares for datapoints
    legend
      .selectAll("rect")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", 0)
      .attr("y", function (d, i) {
        return 10 + (i * 20 * cfg.svg_scale);
      })
      .attr("width", 10 * cfg.svg_scale)
      .attr("height", 10 * cfg.svg_scale)
      .style("fill", function (d, i) {
        return cfg.color(i);
      });

    // Create text of data values next to squares
    legend
      .selectAll("text")
      .data(data)
      .enter()
      .append("text")
      .attr("class", "text")
      .attr("x", 15 * cfg.svg_scale)
      .attr("y", function (d, i) {
        return 11 + 9 * cfg.svg_scale + (i * 20 * cfg.svg_scale);
      })
      .style("font-family", "sans-serif")
      .style("font-size", 10 + 5 * cfg.text_scale + "px")
      .text(function (d) {
        return d;
      });




  }
};
